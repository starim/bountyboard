source 'https://rubygems.org'

gem 'rails', '~> 3.2'

# Bundle edge Rails instead:
# gem 'rails', :git => 'git://github.com/rails/rails.git'


# Gems used in the production environment.
group :production do
  gem 'unicorn'
  gem 'execjs'
  gem 'therubyracer', :platforms => :ruby
  gem 'dalli'
  gem 'dotenv'
end


# Gems used in the development environment.
group :development do
  gem 'guard'
  gem 'ruby-growl'
  gem 'annotate', '~> 2.6.5'
  # debugger doesn't work with Ruby 2.0 and later,
  # and the gem's maintainers have no plans to fix it;
  # byebug was forked to work with Ruby 2+
  if RUBY_VERSION < "2.0" then
    gem 'debugger'
  else
    gem 'byebug'
  end

  gem 'rubocop'
end


# Gems used only for assets and not required
# in production environments by default.
group :assets do
  gem 'sass-rails',   '~> 3.2'
  gem 'jquery-ui-sass-rails'
  gem 'coffee-rails', '~> 3.2'

  # See https://github.com/sstephenson/execjs#readme for more supported runtimes
  # gem 'therubyracer', :platforms => :ruby

  gem 'uglifier', '>= 1.0.3'
end


# Gems used when running tests.
group :test do
  gem 'rspec-rails', '~> 2.99'
  # uncomment this gem when upgrading to rspec 3
  # gem 'rspec-its'
  gem 'capybara'
  gem 'rake'
  gem 'email_spec'
  gem 'timecop'
end

group :development, :test do
  gem 'factory_girl_rails'
  gem 'faker'
end


# Universal gems that should be used in all environments.
gem 'pg'
gem 'foreigner'
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'masonry-rails'
gem 'normalize-rails'
gem 'omniauth-browserid'
gem 'omniauth-deviantart'
# later versions of money-rails introduce breaking API changes
gem 'money-rails', '0.9'
gem 'formtastic'
gem 'sanitize'
gem 'turbolinks'
gem 'strong_parameters'
gem "paperclip", "~> 4.1"
gem 'kaminari'
gem 'whenever'
gem 'postgres_ext'
gem 'pagedown-rails'
gem 'redcarpet'
gem 'paranoia', '~> 1.3'


# To use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# To use Jbuilder templates for JSON
# gem 'jbuilder'

# Use unicorn as the app server
# gem 'unicorn'

# Deploy with Capistrano
# gem 'capistrano'

