The Bounty Board
==

The Bounty Board is a project begun in 2013, aimed at connecting
artists with the people who love their work. It is a marketplace where fans can
post requests for art along with the price they will pay ("the bounty"), and
the site's artists can accept and complete these bounties.  

This software targets Ruby 2 and Rails 3 (with work to convert to Rails 4 in
the `ruby-rails-upgrade` branch).  

As of the end of 2014, work on the Bounty Board has stopped and this project is
now abandoned. It is available for anyone to use, learn from, or even continue
developing under the terms of the AGPL license (see LICENSE.txt).  


Contributors
--

We gladly accept pull requests, but new code should conform to the [Ruby style
guide](https://github.com/bbatsov/ruby-style-guide). All contributions should
also have tests written covering any new functionality or changes in
functionality.


Setup Instructions
--
This project requires the following external programs to be installed:
    * ImageMagick
    * PostgreSQL
