Bountyboard::Application.routes.draw do

  root :to => 'Bounties#index'

  resources :artists
  resources :bounties, shallow: true do
    resources :comments
    member do
      get 'complete'
    end
  end

  # these resources are managed automatically by the system, so we don't need
  # user-accessible index, new, or edit forms
  resources :candidacies, :except => [:index, :new, :edit]
  resources :votes, :except => [:index, :new, :edit]
  resources :favorites, :except => [:index, :new, :edit]

  resources :users, :except => [:index] # forbid listing users

  # authentication routes
  match '/auth/:provider/callback', to: 'Sessions#create'
  match '/sign_out', to: 'Sessions#destroy', via: [ :delete, :post ]
  match '/auth/failure', to: 'Sessions#auth_failure'
end

