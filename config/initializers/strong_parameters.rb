# Automatically add Strong Parameter support to model classes. This whole file
# is unnecessary in Rails 4, but is necessary when using Strong Parameters in
# Rails 3.
ActiveRecord::Base.send(:include, ActiveModel::ForbiddenAttributesProtection)

