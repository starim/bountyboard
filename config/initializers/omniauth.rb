# -*- encoding : utf-8 -*-
require 'omniauth-browserid'
require 'omniauth-deviantart'

# send log messages from our authentication system to the Rails log
OmniAuth.config.logger = Rails.logger

# uncomment the following section to test logging in as an artist, or another
# user
#
#OmniAuth.config.test_mode = true
#OmniAuth.config.mock_auth[:browser_id] = OmniAuth::AuthHash.new({
#  :provider => 'browserid',
#  :uid => 'admin1@test.com'
#})

Rails.application.config.middleware.use OmniAuth::Builder do
  provider :browser_id
end

