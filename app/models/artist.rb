# == Schema Information
#
# Table name: users
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  email              :citext           not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  deleted_at         :datetime
#  type               :string(255)
#  bio                :text             default(""), not null
#  bounty_rules       :text             default(""), not null
#  approved           :boolean          default(FALSE), not null
#  admin              :boolean          default(FALSE), not null
#  badge_file_name    :string(255)
#  badge_content_type :string(255)
#  badge_file_size    :integer
#  badge_updated_at   :datetime
#

class Artist < User

  # Relationships ==============================================================
  has_many :candidacies, :inverse_of => :artist
  has_many :bounties, :through => :candidacies

  # Paperclip gem.
  has_attached_file :badge, :styles => { :standard => "280x200>",
    :default_url => "/images/Badge.png" }

  # Validations ================================================================
  validates :bio, presence: true
  validates :bounty_rules, presence: true
  validates :approved, :inclusion => {:in => [true, false]}

  # The "name" property is optional for users but required for artists
  validates :name, presence: true

  acts_as_paranoid


  # Methods ====================================================================

  # parses the markdown in this artist's bio property into safe HTML so that it
  # can be used directly in a view
  def bio_to_html
    parse_markdown(self.bio)
  end

  # parses the markdown in this artist's bounty_rules property into safe HTML
  # so that it can be used directly in a view
  def bounty_rules_to_html
    parse_markdown(self.bounty_rules)
  end


  # Artist query filters =======================================================
  #
  # Use these to get specific subsets of bounties. These
  # methods are chainable.
  class << self
    def available()
      where( :approved => true )
    end
  end

end

