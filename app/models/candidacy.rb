# == Schema Information
#
# Table name: candidacies
#
#  id          :integer          not null, primary key
#  bounty_id   :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  artist_id   :integer          not null
#  accepted_at :datetime
#

class Candidacy < ActiveRecord::Base

  # Relationships ==============================================================
  belongs_to :artist, :inverse_of => :candidacies
  belongs_to :bounty, :inverse_of => :candidacies

  # Validations ================================================================
  validates :artist, presence: true
  validates :bounty, presence: true
  validates :bounty_id, :uniqueness => { :scope => :artist_id }
  validate :validate_one_acceptor
  validate :bounty_owner_cant_be_own_candidate

  # Ensures that each bounty has only one acceptor at most.
  def validate_one_acceptor
    if self.accepted_at

      other_acceptors = Candidacy.where(
        'bounty_id=? AND accepted_at IS NOT NULL',
        self.bounty_id
      )

      # if this bounty has already been saved, it'll show up in the
      # other_acceptors list so exclude it explicitly here
      if self.id
        other_acceptors = other_acceptors.where('id<>?', self.id)
      end

      if other_acceptors.count > 0
        errors.add(
          :accepted_at,
          "can't be set to true because an acceptor already exists for this bounty"
        )
      end
    end
  end

  def bounty_owner_cant_be_own_candidate
    if self.bounty && self.artist
      if self.bounty.owner == self.artist
        errors.add(
          :artist,
          "can't be a candidate for a bounty that he or she owns"
        )
      end
    end
  end
end
