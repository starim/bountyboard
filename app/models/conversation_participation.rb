# == Schema Information
#
# Table name: conversation_participations
#
#  id              :integer          not null, primary key
#  conversation_id :integer          not null
#  user_id         :integer          not null
#

class ConversationParticipation < ActiveRecord::Base
  belongs_to :conversation,
    :inverse_of => :conversation_participations
  belongs_to :user,
    :inverse_of => :conversation_participations
  has_many :messages

  validates :conversation, presence: true
  validates :user,         presence: true

  validates_uniqueness_of :user_id, :scope => :conversation_id
end

