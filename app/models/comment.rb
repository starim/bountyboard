# == Schema Information
#
# Table name: comments
#
#  id         :integer          not null, primary key
#  bounty_id  :integer          not null
#  user_id    :integer          not null
#  content    :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Comment < ActiveRecord::Base
  belongs_to :bounty, :inverse_of => :comments
  belongs_to :user,   :inverse_of => :comments

  validates :bounty,   presence: true
  validates :user,     presence: true
  validates :content,  presence: true

  # override the "spacheship operator" so that default sorting will sort
  # primarily by creation date, and secondarily by user and bounty
  def <=>(other)
    [self.created_at, self.bounty, self.user] <=>
      [other.created_at, other.bounty, other.user]
  end
end

