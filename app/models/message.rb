# == Schema Information
#
# Table name: messages
#
#  id                            :integer          not null, primary key
#  conversation_participation_id :integer          not null
#  content                       :text             not null
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#

class Message < ActiveRecord::Base
  belongs_to :conversation_participation, :inverse_of => :messages
  has_one :conversation,
    :through => :conversation_participation,
    :inverse_of => :messages
  has_one :user,
    :through => :conversation_participation,
    :inverse_of => :messages

  validates :conversation_participation, presence: true
  validates :content, presence: true

  # override the "spacheship operator" so that default sorting will sort
  # primarily by creation date, and secondarily by participation ID
  def <=>(other)
    [self.created_at, self.conversation_participation] <=>
      [other.created_at, other.conversation_participation]
  end

end

