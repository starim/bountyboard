# == Schema Information
#
# Table name: votes
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  bounty_id  :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  vote_type  :boolean          default(FALSE), not null
#

class Vote < ActiveRecord::Base

  # Relationships ==============================================================
  belongs_to :user, :inverse_of => :votes
  belongs_to :bounty, :inverse_of => :votes

  # Validations ================================================================
  validates :user, presence: true
  validates :bounty, presence: true
  validates_uniqueness_of :user_id, :scope => :bounty_id

  validate :only_allowed_users_may_vote

  def only_allowed_users_may_vote
    # note that this validation might run before the presence validation for
    # bounty or user, so let those validations catch if bounty or user is nil
    if self.bounty && self.user
      if self.bounty.owner == self.user
        errors.add(:user, 'cannot vote on a bounty he or she owns.')
      else
        other_votes_with_this_user = self.bounty.votes.select { |vote|
          vote.user == user && vote != self
        }
        if other_votes_with_this_user.length > 0
          errors.add(:user, 'has already voted on this bounty.')
        end
      end
    end
  end
end
