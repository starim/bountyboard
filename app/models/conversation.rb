# == Schema Information
#
# Table name: conversations
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Conversation < ActiveRecord::Base
  has_many :conversation_participations,
    :dependent => :destroy,
    :inverse_of => :conversation
  has_many :users,
    :through => :conversation_participations
  has_many :messages,
    :through => :conversation_participations,
    :order => "created_at ASC"

  validates_presence_of :conversation_participations
  validates_length_of   :conversation_participations, :minimum => 2
end

