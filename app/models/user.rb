# == Schema Information
#
# Table name: users
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  email              :citext           not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  deleted_at         :datetime
#  type               :string(255)
#  bio                :text             default(""), not null
#  bounty_rules       :text             default(""), not null
#  approved           :boolean          default(FALSE), not null
#  admin              :boolean          default(FALSE), not null
#  badge_file_name    :string(255)
#  badge_content_type :string(255)
#  badge_file_size    :integer
#  badge_updated_at   :datetime
#

class User < ActiveRecord::Base

  # Relationships ==============================================================
  has_many :votes, :dependent => :destroy, :inverse_of => :user
  has_many :owned_bounties, {
    :foreign_key => "user_id",
    :class_name => "Bounty",
    :inverse_of => :owner
  }
  has_many :favorites, :inverse_of => :user
  has_many :favorite_bounties, :through => :favorites, :source => :bounty
  has_many :comments, :dependent => :destroy, :inverse_of => :user
  has_many :conversation_participations, :inverse_of => :user
  has_many :conversations, :through => :conversation_participations
  has_many :messages,
    :through => :conversation_participations,
    :inverse_of => :user

  # Validations ================================================================
  validates :email, presence: true
  validates_uniqueness_of :email, :case_sensitive => false

  acts_as_paranoid


  # Returns the name of the user if provided, email if not.
  def get_identifier
    return self[:name] ? self[:name] : self[:email]
  end

  # Returns true if the user is an admin, false if not.
  def admin?
    return self.admin
  end
end
