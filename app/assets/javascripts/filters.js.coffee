initializeFilters = ->
  if $("#dropdown-filters-controls").length > 0

    # normally the button to activate the filter controls is hidden, but now
    # that we're activating the filter controls, make it visible to the user
    $("#filters-controls-toggle").show()

    # wait for update requests from slider controls to subside for
    # at least this many milliseconds before actually updating to
    # prevent sliding the control from generating hundreds of
    # refresh requests
    SLIDER_DELAY = 300

    # parameters passed to the back end for filtering are stored
    # here, indexed by the property being filtered (i.e.
    # filter_parameters["price_min"] = 30)
    filter_parameters = []
    apply_filters = ->
      backend_url = window.location.pathname

      console.log "Sending the following bounty filters to \"#{backend_url}\""
      console.log filter_parameters

      sanitized_parameters = []
      for param of filter_parameters
        key = encodeURIComponent(param)
        value = encodeURIComponent(filter_parameters[param])
        sanitized_parameters.push(key + "=" + value)
      $(".bounty-list").load(
        # the request returns the whole page's HTML, only keep the bounty list
        # part (thus the '.bounty-list' appended to the URL)
        "#{backend_url} .bounty-list",  
        sanitized_parameters.join("&"),
        window.refresh_bounties
      )

    # use this if your filter is mutually exclusive to another filter
    # and needs to disable it (note that this only disables
    # buttonset-based filters currently)
    last_value = {}
    disable_filter = (filter, state_to_set) ->
      filter_inputs = $("#filter_#{filter}")
      # since some filters are only displayed to certain users, check
      # if the specified filter even exists before proceeding
      if filter_inputs.length == 0
        return
      delete filter_parameters[filter]
      if not last_value[filter]?
        last_value[filter] = filter_inputs.find(":checked").val()
      filter_inputs.find(
        "input[value='#{state_to_set}']"
      ).prop("checked", true)
      filter_inputs.find("input").button( disabled: true )
      filter_inputs.buttonset("refresh")

    reenable_filter = (filter) ->
      filter_inputs = $("#filter_#{filter}")
      # since some filters are only displayed to certain users, check
      # if the specified filter even exists before proceeding
      if filter_inputs.length == 0
        return

      if last_value[filter]? and last_value[filter] != ""
        filter_parameters[filter] = last_value[filter]
      filter_inputs.find("input").button( disabled: false )
      filter_inputs.find(
        "input[value='#{last_value[filter]}']"
      ).prop("checked", true)
      filter_inputs.buttonset("refresh")



    # initialize filters controls

    # price filter

    # The price filter uses an exponential scale, since fine dollar-by-dollar
    # control isn't needed in the $10,000 range, but is in the $10 range. These
    # methods provide conversion routines from dollars to exponential scale and
    # back.
    dollars_to_scale = (dollars) ->
      if dollars > 0
        Math.log(dollars)
      else
        0
    scale_to_dollars = (scale) ->
      Math.pow(Math.E, scale)

    price_filter_attrs = $("#filter_cost").data()
    bounty_min_price = parseFloat(price_filter_attrs.bounty_min_price)
    bounty_min_price_scaled = dollars_to_scale(bounty_min_price)
    bounty_max_price = parseFloat(price_filter_attrs.bounty_max_price)
    bounty_max_price_scaled = dollars_to_scale(bounty_max_price)

    # updates the price filter display
    # lower and upper bound are the upper and lower bound of the price range
    # selected (both parameters should be in actual dollars and not the
    # exponential scale used by the slider control)
    # the reload parameter is a boolean specifying whether the page
    # should be reloaded with the new filters, defaulting to true
    price_update_timer = null
    price_filter_update = (lower_bound, upper_bound, reload=true) ->
      $("#filter_cost").val("$#{lower_bound} - $#{upper_bound}")

      if reload
        if price_update_timer != null
          clearTimeout price_update_timer
        complete_update = ->
          price_update_timer = null
          filter_parameters["price_min"] = lower_bound
          filter_parameters["price_max"] = upper_bound
          apply_filters()
        price_update_timer = setTimeout complete_update, SLIDER_DELAY

    $("#filter_cost_widget").slider
      range: true
      min: bounty_min_price_scaled
      max: bounty_max_price_scaled
      step: 0.01
      values: [bounty_min_price_scaled, bounty_max_price_scaled]
      slide: (event, ui) ->
        # round to whole dollars for a prettier display
        lower_bound = Math.floor(scale_to_dollars(ui.values[0]))
        upper_bound = Math.ceil(scale_to_dollars(ui.values[1]))
        price_filter_update(lower_bound, upper_bound)

    # initialize the price display
    price_filter_update(bounty_min_price, bounty_max_price, false)


    # adult content filter

    $("#filter_adult").buttonset()
    $("#filter_adult").change ->
      filter_parameters["adult"] = $("#filter_adult :checked").val()
      apply_filters()


    # bounties you own

    $("#filter_own").buttonset()
    $("#filter_own").change ->
      own_value = $("#filter_own :checked").val()
      if own_value is ""
        delete filter_parameters["own"]
      else
        filter_parameters["own"] = own_value
      apply_filters()


    # bounties you may complete

    $("#filter_candidacy").buttonset()
    status_filter = $("#filter_status")
    own_filter = $("#filter_own")
    last_status_value = status_filter.find(":checked").val()
    last_own_value = own_filter.find(":checked").val()
    $("#filter_candidacy").change ->
      may_accept_value = $("#filter_candidacy :checked").val()
      if may_accept_value is ""
        delete filter_parameters["may_accept"]
        reenable_filter("status")
        reenable_filter("own")
        reenable_filter("artist_workload")
        reenable_filter("artist_portfolio")
      else
        filter_parameters["may_accept"] = may_accept_value
        disable_filter("status", "unclaimed")
        disable_filter("own", "")
        disable_filter("artist_workload", "")
        disable_filter("artist_portfolio", "")

      apply_filters()


    # status filter

    $("#filter_status").buttonset()
    $("#filter_status").change ->
      allowed_status = $("#filter_status :checked").val()
      if allowed_status is ""
        delete filter_parameters["status"]
      else
        filter_parameters["status"] = allowed_status
      apply_filters()


    # artist workload filter

    $("#filter_artist_workload").buttonset()
    $("#filter_artist_workload").change ->
      filter_value = $("#filter_artist_workload :checked").val()
      if filter_value is ""
        delete filter_parameters["artist_workload"]
        reenable_filter("status")
        reenable_filter("own")
        reenable_filter("candidacy")
        reenable_filter("artist_portfolio")
      else
        filter_parameters["artist_workload"] = filter_value
        disable_filter("status", "accepted")
        disable_filter("own", "")
        disable_filter("candidacy", "")
        disable_filter("artist_portfolio", "")

      apply_filters()


    # artist portfolio filter

    $("#filter_artist_portfolio").buttonset()
    $("#filter_artist_portfolio").change ->
      filter_value = $("#filter_artist_portfolio :checked").val()
      if filter_value is ""
        delete filter_parameters["artist_portfolio"]
        reenable_filter("status")
        reenable_filter("own")
        reenable_filter("candidacy")
        reenable_filter("artist_workload")
      else
        filter_parameters["artist_portfolio"] = filter_value
        disable_filter("status", "completed")
        disable_filter("own", "")
        disable_filter("candidacy", "")
        disable_filter("artist_workload", "")
      apply_filters()


$(document).ready initializeFilters
$(document).on "page:load", initializeFilters

