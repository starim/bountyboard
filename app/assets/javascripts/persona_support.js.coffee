initializePersona = ->

  $(".persona-login-button").button()
  $(".persona-logout-button").button()

  navigator.id.watch(
    onlogin: (assertion) ->

      console.log "Persona onlogin callback activated."

      # if the user is already logged in, don't re-login
      # note that this detection logic assumes there is at least one login
      # button on the page when a user isn't logged in
      if($('.persona-login-button').length == 0)
        return

      $.ajax(
        type: "POST"
        url: "/auth/browser_id/callback"
        beforeSend: (xhr) ->
          xhr.setRequestHeader("Accept", "application/json")
          $('.persona-buttons').hide()
          $('.persona-spinner-text').text('Logging In...')
          $('.persona-ajax-spinner').show()
        data: { assertion: assertion }
        success: ->
          location.reload()
        error: (xhr, status_string, error_message) ->
          console.log "Failure function called after login POST.\n#{status_string}: #{error_message}"
          $('.persona-ajax-spinner').hide()
          $('.persona-buttons').show()
      )

    onlogout: ->

      console.log "Persona onlogout callback activated."

      # if the user isn't logged in, don't bother trying to log out
      # note that this detection logic assumes there is at least one login
      # button on the page when a user isn't logged in

      if($('.persona-logout-button').length == 0)
        return

      $.ajax(
        type: "POST" # should be DELETE to be RESTful but not well-supported by browsers
        url: "/sign_out"
        beforeSend: (xhr) ->
          xhr.setRequestHeader("Accept", "application/json")
          $('.persona-buttons').hide()
          $('.persona-spinner-text').text('Logging Out...')
          $('.persona-ajax-spinner').show()
        success: ->
          location.reload()
        error: (xhr, status_string, error_message) ->
          console.log "Failure function called after logout POST.\n#{status_string}: #{error_message}"
          $('persona-ajax-spinner').hide()
          $('.persona-buttons').show()
      )
  )

  # attach a click handler to login buttons that initiates the Persona login
  # process
  $('.persona-login-button').click ->
    # TODO: add a siteLogo: "URL" property to show the user our site's logo
    # when logging in with Persona
    # note that Persona REQUIRES https on the hosting site when loading logos
    navigator.id.request(siteName: "The Bounty Board")

  # attach a click handler to logout buttons that initiates the Persona logout
  # process
  $('.persona-logout-button').click ->
    navigator.id.logout()

$(document).ready initializePersona
$(document).on "page:load", initializePersona

