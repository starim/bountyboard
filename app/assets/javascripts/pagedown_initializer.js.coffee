# initialize the Pagedown WYSIWYG editor
$(document).ready ->
  converter = Markdown.getSanitizingConverter()
  $(".wysiwyg-panel").each ->

    idsuffix = $(this).data("idsuffix")
    custom_class = $(this).data("preview-window-class") ? ""
    custom_title = $(this).data("title") ? "Preview"

    # create the preview window; this has to exist outside of any other
    # elements so that their CSS styles don't pollute the preview
    preview_container = $("
<div class='wmd-preview-container' id='wmd-preview-container#{idsuffix}'>
  <div class='wmd-preview #{custom_class}' id='wmd-preview#{idsuffix}'>
  </div>
</div>
    ").appendTo("body")

    ignore_next_preview_popup = false

    # now turn the preview window into a jQuery-UI dialog popup
    input_field = $(".wmd-input#{idsuffix}")
    preview_container.dialog
      autoOpen: false,
      position: "left",
      title: custom_title,
      width: ($(document).width() - input_field.width()) / 2,

    input_field.focus ->
      if ignore_next_preview_popup
        ignore_next_preview_popup = false
        return false
      preview_container.dialog "open" 

    input_field.blur ->
      # Wait a moment so we can see who got focus, then close the
      # dialog window. When a jQuery UI dialog closes, it
      # automatically gives focus to the input that opened it, so we
      # need to manually hand focus back to the correct element after
      # closing the dialog.
      if preview_container.dialog "isOpen"
        setTimeout ( ->
          new_focus = $(document.activeElement)
          # because this will hand focus back to the input that opens
          # the preview popup on focus, we need to tell it not to do
          # that this one time or else we'll get an infinite loop
          # between the focus and blur events as they alternately
          # open and close the preview popup
          ignore_next_preview_popup = true
          preview_container.dialog "close"
          new_focus.focus()
        ), 1

    editor = new Markdown.Editor converter, idsuffix
    editor.run()

