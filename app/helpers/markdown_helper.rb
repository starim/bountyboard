module MarkdownHelper
  def parse_markdown(text, options: nil, extensions: nil)

    options ||= {
      filter_html:     true,
      no_styles:       true,
      safe_links_only: true,
      hard_wrap:       true, 
      link_attributes: { rel: 'nofollow', target: "_blank" }
    }

    extensions ||= {
      autolink:           true,
      superscript:        true,
      strikethrough:      true
    }

    renderer = Redcarpet::Render::HTML.new(options)
    markdown = Redcarpet::Markdown.new(renderer, extensions)

    markdown.render(text).html_safe
  end
end

