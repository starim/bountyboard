module FiltersHelper

  # This helper is designed for controllers that want to filter a list of
  # bounties. It takes care of all the logic required to turn a parameter list
  # of filter contraints into an actual filtered list of bounties, hiding away
  # the details of the process and putting in place safe defaults (i.e. private
  # bounties are properly hidden unless specifically told to show them).
  #
  # parameters:
  #
  #   bounties:
  #     the bounty list to filter (Bounty.all if you want to start with a list
  #     of all bounties, or Bounty.where("some condition") if you want to
  #     filter a custom list of bounties
  #
  #   custom_filters:
  #     a hash of filters to override the default filter set; possible values
  #     are given below (i.e. { :adult => 'kid-friendly', :price_max => 700 }
  #     to show only non-adult bounties whose price is less than $7)
  #
  #
  # filter values:
  #
  #   A nil value turns off a filter, except for the viewable_by filter which
  #   is always on (see its description for how it handles nil).
  #
  #   price_min (float):
  #     Bounties with prices less than this value in cents will be removed. Off
  #     by default.
  #
  #   price_max (float):
  #     Bounties with prices more than this value in cents will be removed. Off
  #     by default.
  #
  #   adult (string):
  #     "adult" shows only adult-rated bounties
  #     "kid-friendly" removes all adult-rated bounties
  #     "kid-friendly unless viewer involved with bounty" functions like
  #     "kid-friendly" except it still shows adult bounties the user is involed
  #     with (owns, accepted, or is a candidate for). This is the default
  #     setting.
  #
  #   viewable_by (User):
  #     Show only bounties viewable by the specified User object, or only
  #     viewable by guests if User is nil. This filter is always on to protect
  #     the privacy of our users. If you need a bounty list that isn't
  #     concerned with privacy you'll have to invoke the Bounty filters
  #     directly. This filter's value defaults to the value returned by the
  #     current_user helper.
  #
  #   own (User):
  #     Show only bounties owned by the specified user object. Off by default.
  #
  #   candidacy (string):
  #     Show only bounties that the named artist is eligible to accept. If this
  #     artist doesn't exist then all bounties will be filtered out. Off by
  #     default.
  #
  #   status (string):
  #     Show only bounties with the named status ("Unclaimed", "Accepted",
  #     "Completed", or "Invalid"). The comparison in status name is
  #     case-insensitive. Off by default.
  #
  #   artist_workload (string):
  #     Show only bounties currently accepted by the named artist but not
  #     completed. If the given name doesn't correspond to any artist in the
  #     system then all bounties will be filtered out. Off by default.
  #
  #   artist_portfolio (string):
  #     Show only bounties completed by the named artist. If the given name
  #     doesn't correspond to any artist in the system then all bounties will
  #     be filtered out. Off by default.
  #
  def apply_filters(bounties, custom_filters={})

    # default filter values
    filters =  {
      :price_min => nil,
      :price_max => nil,
      :adult => "kid-friendly unless viewer involved with bounty",
      :viewable_by => current_user,
      :own => nil,
      :candidacy => nil,
      :status => nil,
      :artist_workload => nil,
      :artist_portfolio => nil
    }.with_indifferent_access

    filters.merge!(custom_filters)

    # the viewable_by filter is always applied
    bounties = bounties.viewable_by(filters[:viewable_by])

    if filters[:price_min]
      bounties = bounties.price_greater_than params["price_min"].to_f
    end
    if filters[:price_max]
      bounties = bounties.price_less_than params["price_max"].to_f
    end
    if filters[:adult] == "adult"
      bounties = bounties.only_adult_content
    elsif filters[:adult] == "kid-friendly"
      bounties = bounties.no_adult_content
    elsif filters[:adult] == "kid-friendly unless viewer involved with bounty"
      bounties = bounties.no_adult_content_unless_associated_with_user(current_user)
    end
    if filters[:own]
      bounties = bounties.owned_by(filters[:own])
    end
    if filters[:candidacy]
      bounties = bounties.may_accept(
        Artist.where(:name => filters[:candidacy]).first
      )
    end
    if filters[:status]
      bounties = bounties.only_status(filters[:status])
    end
    if filters[:artist_workload]
      bounties = bounties.
        only_acceptor(
          Artist.where(:name => filters[:artist_workload]).first
        ).
        only_accepted
    end
    if filters[:artist_portfolio]
      bounties = bounties.
        only_acceptor(
          Artist.where(:name => filters[:artist_portfolio]).first
        ).
        only_completed
    end

    bounties
  end

end

