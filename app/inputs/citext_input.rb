# tell Formatstic that model properties with the PostgreSQL data type
# of citext (case-insensitive text) should be treated as strings
class CitextInput < Formtastic::Inputs::StringInput
end

