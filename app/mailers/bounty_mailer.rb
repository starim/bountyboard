class BountyMailer < ActionMailer::Base
  default from: "no-reply@example.com"

  def acceptance_email(bounty)
    @bounty = bounty
    @artist = bounty.acceptor_candidacy.artist
    mail(to: format_email_recipient(bounty.owner),
            subject: "Your bounty has been accepted")
  end

  def unacceptance_email(bounty, unaccepting_artist)
    @bounty = bounty
    @unaccepting_artist = unaccepting_artist

    if !@bounty.artists.include? @unaccepting_artist
      raise <<-error_message
        Invalid unacceptance email request: artist is not a candidate for
        the specified bounty and therefore cannot un-accept the
        bounty.
        Un-accepting Artist:
        #{@unaccepting_artist.inspect}
        Bounty:
        #{@bounty.inspect}
        error_message
    end

    mail(to: format_email_recipient(bounty.owner),
            subject: "Work has stopped on your bounty")
  end

  def rejection_email(bounty)
    @bounty = bounty
    mail(to: format_email_recipient(bounty.owner),
            subject: "Your bounty has been rejected")
  end

  def completion_email(bounty)
    @bounty = bounty
    @artist = bounty.acceptor_candidacy.artist
    mail(to: format_email_recipient(bounty.owner), subject: "Your bounty is complete")
  end

  private

  def format_email_recipient(person)
    person.name ? "#{person.name} <#{person.email}>" : person.email
  end
end
