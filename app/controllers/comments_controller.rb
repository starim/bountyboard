class CommentsController < ApplicationController

  def create
    @comment = Comment.new(comment_create_params)
    @comment.bounty_id = params[:bounty_id]
    @comment.content =
      Sanitize.clean(@comment.content, Sanitize::Config::RELAXED)
    @comment.user = current_user
    if @comment.save
      if request.env['HTTP_REFERER']
        redirect_to :back
      else
        redirect_to root_url
      end
    else
      logger.error "Could not save new comment #{@comment.inspect} due to the following errors:\n#{@comment.errors.full_messages}"
      flash[:error] = "Error saving comment."
      if request.env['HTTP_REFERER']
        redirect_to :back
      else
        redirect_to root_url
      end
    end
  end

  def update
    @comment = Comment.find(params[:id])
    if @comment
      if @comment.user != current_user
        flash[:error] = "You are not allowed to edit that comment."
        if request.env['HTTP_REFERER']
          redirect_to :back
        else
          redirect_to root_url
        end
        return
      end

      @comment.update_attributes(comment_update_params)
      if !@comment.save
        flash[:error] = "Error updating the comment."
      end
    end

    if request.env['HTTP_REFERER']
      redirect_to :back
    else
      redirect_to root_url
    end
  end

  def destroy
    @comment = Comment.find(:id => params[:id])
    if @comment
      if !(@comment.user == current_user || current_user.admin?)
        flash[:error] = "You are not allowed to remove that comment."
        if request.env['HTTP_REFERER']
          redirect_to :back
        else
          redirect_to root_url
        end
        return
      end
      if !@comment.destroy
        flash[:error] = "Error removing the comment."
      end
    end

    if request.env['HTTP_REFERER']
      redirect_to :back
    else
      redirect_to root_url
    end
  end

  private

    def comment_create_params
      params.require(:comment).permit(:content)
    end

    def comment_update_params
      params.require(:comment).permit(:content)
    end
end

