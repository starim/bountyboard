class SessionsController < ApplicationController

  skip_before_filter :require_login, :except => [:destroy]

  def create

    respond_to do |format|
      auth_info = request.env['omniauth.auth']

      if !auth_info.uid
        handle_auth_failure("invalid credentials")
        return
      end

      user = User.where(:email => auth_info.uid).first
      if !user
        # if this user has never visited before, create a user entry for them
        user = User.new(email: auth_info.uid)
        if !user.save
          handle_auth_failure(auth_info, "this appears to be your first visit to our site, but our database rejected your account (#{user.errors.full_messages})")
          return
        end
      end

      session[:user_id] = user

      format.html { redirect_to root_path, :status => 303 }
      format.json { render json: {} }
    end
  end

  def destroy()

    session[:user_id] = nil

    respond_to do |format|
      format.html do
        if request.env['HTTP_REFERER']
          redirect_to :back
        else
          redirect_to root_url
        end
      end
      format.json { render json: {} }
    end
  end

  # Omniauth calls this method when a user's sign-in process fails while
  # Omniauth is handling the user
  def auth_failure()
    # make the OmniAuth message slightly more user-friendly by replacing
    # underscores with spaces
    handle_auth_failure(params[:message].gsub(/_/, ' '))
  end

  private

    # this method handles failed sign-in by displaying to the user why their
    # sign-in process failed
    def handle_auth_failure(message)
      flash[:error] = "Sign-in failed: #{message}"
      redirect_to root_path, :status => 303
    end
end

