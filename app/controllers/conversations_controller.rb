class ConversationsController < ApplicationController

  # TODO ensure that if user receives an error while sending a message, that user's entered text is not cleared. Handle post/put calls asynchronously?
  def create
    @conversation = Conversation.new()
    @conversation.conversation_participation <<
      ConversationParticipation.new(:user_id => current_user)

    if params[:to].is_a? Array
      params[:to].each do |to_user|
        @conversation.conversation_participation <<
          ConversationParticipation.new(:user_id => to_user)
      end
    else
      @conversation.conversation_participation <<
        ConversationParticipation.new(:user_id => params[:to])
    end

    if !@conversation.save
      flash[:error] = "Error saving conversation."
    end

    if request.env['HTTP_REFERER']
      redirect_to :back
    else
      redirect_to root_url
    end
  end
end

