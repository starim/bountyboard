class MessagesController < ApplicationController

  def create
    @message = Message.new(message_create_params)
    if params[:to]
      @message.conversation_participation = ConversationParticipation.where(
        :user => current_user,
        :conversation_id => params[:conversation_id]
      )
    else
    end

    if !@message.save
      flash[:error] = "Error saving message."
    end

    if request.env['HTTP_REFERER']
      redirect_to :back
    else
      redirect_to root_url
    end
  end

  def update
    @message = Message.find(params[:id])
    if @message
      if @message.user != current_user
        flash[:error] = "You are not allowed to edit that message."
        if request.env['HTTP_REFERER']
          redirect_to :back
        else
          redirect_to root_url
        end
        return
      end

      @message.update_attributes(message_update_params)
      if !@message.save
        flash[:error] = "Error updating the message."
      end
    end

    if request.env['HTTP_REFERER']
      redirect_to :back
    else
      redirect_to root_url
    end
  end

  def destroy
    @message = Message.find(:id => params[:id])
    if @message
      if @message.user != current_user
        flash[:error] = "You are not allowed to remove that message."
        if request.env['HTTP_REFERER']
          redirect_to :back
        else
          redirect_to root_url
        end
        return
      end

      # to avoid having empty conversation objects taking up space in the
      # database and cluttering the user's conversations display, if this is
      # the last message in the conversation then delete the whole conversation
      conversation = @message.conversation_participation.conversation
      if conversation.messages.length == 1
        if !conversation.destroy
          flash[:error] = "Error removing the empty conversation."
        end
      else
        if !@message.destroy
          flash[:error] = "Error removing the message."
        end
      end
    end

    if request.env['HTTP_REFERER']
      redirect_to :back
    else
      redirect_to root_url
    end
  end

  private

    def message_create_params
      params.require(:message).permit(:content)
    end

    def message_update_params
      params.require(:message).permit(:content)
    end
end

