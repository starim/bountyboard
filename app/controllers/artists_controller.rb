class ArtistsController < ApplicationController

  include FiltersHelper

  skip_before_filter :require_login, :only => [:index, :show]

  # the maximum number of bounties that can be displayed in each
  # section of the artist dashboard
  def self.BOUNTIES_PER_SECTION
    20
  end

  # Display a list of all artists in the system, with links to see their
  # individual profiles.
  def index
    # TODO show inactive artists?
    @artists = Artist.where("approved = ?", true).order("name")
  end

  # Display an individual artist's profile.
  def show
    # TODO show inactive artists?
    if Artist.exists?(:id => params[:id])
      @artist = Artist.find(params[:id])

      @owner_artist_visiting = (current_user == @artist)
      @admin_visiting = current_user && current_user.admin?

      # if the user has uploaded an image then use it, otherwise use a
      # placeholder
      if @artist.badge.file?
        @badge_url = @artist.badge.url
      else
        @badge_url = 'Badge.png'
      end

    # now use the filter system to get a list of accepted and completed
    # bounties to display

    @completed_bounties = apply_filters(
      Bounty,
      { artist_portfolio: @artist.name }
    ).last(ArtistsController.BOUNTIES_PER_SECTION).sort!

    @accepted_bounties = apply_filters(
      Bounty,
      { artist_workload: @artist.name }
    ).last(ArtistsController.BOUNTIES_PER_SECTION).sort!
    else
      flash[:error] = "That artist does not exist."
      if request.env['HTTP_REFERER']
        redirect_to :back
      else
        redirect_to root_url
      end
    end
  end

  # Display the form to create a new artist. Only admins may perform this
  # action.
  def new
    if current_user.admin?
      @artist = Artist.new
    else
      flash[:error] = "Only administrators can create new artists."
      if request.env['HTTP_REFERER']
        redirect_to :back
      else
        redirect_to root_url
      end
    end
  end

  # Create a new artist. Only admins may perform this action.
  def create
    unless current_user.admin?
      flash[:error] = "Only administrators can create new artists."
      if request.env['HTTP_REFERER']
        redirect_to :back
      else
        redirect_to root_url
      end
      return
    end

    #TODO Figure out how approved fits into our workflow.
    params[:artist][:approved] = true

    @artist = Artist.new(artist_create_params)
    if @artist.save
      flash[:notice] = "Artist created successfully!"
      redirect_to artist_path(@artist)
    else
      flash[:error] = "Error saving artist."
      render 'new'
    end
  end

  # Display the edit form for the currently logged in artist or for an
  # administrator.
  def edit
    if Artist.exists?(:id => params[:id])
      @artist = Artist.find(params[:id])
      unless (@artist == current_user || current_user.admin?)
        flash[:error] = "You are not authorized to edit this artist."
        if request.env['HTTP_REFERER']
          redirect_to :back
        else
          redirect_to root_url
        end
      end
    else
      flash[:error] = "That artist does not exist."
      if request.env['HTTP_REFERER']
        redirect_to :back
      else
        redirect_to root_url
      end
    end
  end

  # Update the currently logged in artist. Admins are allowed to update artist
  # properties.
  def update
    if Artist.exists?(:id => params[:id])
      @artist = Artist.find(params[:id])
      unless (@artist == current_user || current_user.admin?)
        flash[:error] = "You are not authorized to edit this artist."
        if request.env['HTTP_REFERER']
          redirect_to :back
        else
          redirect_to root_url
        end
        return
      end

      if @artist.update_attributes(artist_update_params)
        flash[:notice] = "Artist updated!"
        redirect_to artist_path(@artist)
      else
        flash[:error] = "Error updating artist."
        if request.env['HTTP_REFERER']
          redirect_to :back
        else
          redirect_to root_url
        end
      end
    else
      flash[:error] = "That artist does not exist."
      if request.env['HTTP_REFERER']
        redirect_to :back
      else
        redirect_to root_url
      end
    end
  end

  # Artists are never hard-deleted. Instead, a "deleted_at" property is set,
  # preventing an artist from participating in the system or being selected for
  # bounties.
  def destroy
    @artist = Artist.find(params[:id])
    if current_user.admin?
      artist_name = @artist.name
      if @artist.destroy
        flash[:notice] = "Artist #{artist_name} is now inactive!"
        redirect_to root_path
      else
        flash[:error] = "Error deactivating artist."
        if request.env['HTTP_REFERER']
          redirect_to :back
        else
          redirect_to root_url
        end
      end
    else
      flash[:error] = "Only administrators can delete artists."
      if request.env['HTTP_REFERER']
        redirect_to :back
      else
        redirect_to root_url
      end
    end
  end

  private

    def artist_create_params
      params.require(:artist).permit(
        :name,
        :email,
        :bio,
        :bounty_rules,
        :approved
      )
    end

    def artist_update_params
      params.require(:artist).permit(
        :name,
        :email,
        :bio,
        :bounty_rules
      )

      # admins alone can approve an artist and can set all other artist
      # properties except the artist's name and email, which can only be
      # changed by the artist
      if current_user.admin?
        params.require(:artist).permit(
          :approved,
          :bio,
          :bounty_rules
        )
      end
    end

end
