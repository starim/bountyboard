class FavoritesController < ApplicationController

  def create
    @favorite = Favorite.new(favorite_create_params)
    @favorite.user = current_user
    if !@favorite.save
      flash[:error] = "Error saving favorite."
    end

    if request.env['HTTP_REFERER']
      redirect_to :back
    else
      redirect_to root_url
    end
  end

  def destroy
    if !Favorite.destroy(params[:id])
      flash[:error] = "Error removing favorite."
    end

    if request.env['HTTP_REFERER']
      redirect_to :back
    else
      redirect_to root_url
    end
  end

  private

    def favorite_create_params
      params.require(:favorite).permit(:bounty_id)
    end

end
