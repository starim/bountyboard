class UsersController < ApplicationController

  include FiltersHelper

  respond_to :json, :html

  def show
    # require_login filter already asserts that user is logged in
    if current_user.id != Integer(params[:id]) && !current_user.admin?
      flash[:error] =
        "You aren't allowed to visit a user's page that isn't yours."
      if request.env['HTTP_REFERER']
        redirect_to :back
      else
        redirect_to root_url
      end
      return
    end

    @user = User.find(params[:id])
    if !@user
      flash[:error] = "That user does not exist."
      if request.env['HTTP_REFERER']
        redirect_to :back
      else
        redirect_to root_url
      end
      return
    end

    # apply default filters for this page--these filters are mandatory
    @bounties = Bounty
      .page(params[:page]).per(BountiesController.BOUNTIES_PER_PAGE)

    # Each time the show method is called, the own parameter will be set.
    # So the user dashboard will always only show a user's bounties.
    overrides = { :own => @user }

    # apply filters from the user
    @bounties = apply_filters(@bounties, params.merge(overrides))

    respond_with @bounties.all.sort!.reverse!
  end

end
