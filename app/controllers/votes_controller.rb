class VotesController < ApplicationController

  def create
    @vote = Vote.new(vote_create_params)
    @vote.user = current_user
    if !@vote.save
      flash[:error] = "Error saving vote."
    end

    if request.env['HTTP_REFERER']
      redirect_to :back
    else
      redirect_to root_url
    end
  end

  def destroy
    if !Vote.destroy(params[:id])
      flash[:error] = "Error removing vote."
    end

    if request.env['HTTP_REFERER']
      redirect_to :back
    else
      redirect_to root_url
    end
  end

  private

    def vote_create_params
      params.require(:vote).permit(
        :bounty_id,
        :vote_type
      )
    end

end
