class BountiesController < ApplicationController

  include VotesHelper
  include FiltersHelper
  require 'sanitize'

  respond_to :json, :html

  skip_before_filter :require_login, :only => [:index, :show]

  def self.BOUNTIES_PER_PAGE
    40
  end

  # Display a list of all bounties in the system, with links to see their
  # individual bounties.
  def index

    # apply default filters for this page--these filters are mandatory
    @bounties = Bounty
      .page(params[:page]).per(BountiesController.BOUNTIES_PER_PAGE)

    # if no adult filter is specified, use the "kid-friendly" setting; this is
    # done because the default setting "kid-friendly unless viewer involved
    # with bounty" can't easily be represented as a button in the filter UI,
    # is more computationally expensive, and doesn't provide much benefit for
    # the aggregate view of the bounty index page so we simply don't use that
    # setting on this page
    if !params.has_key?(:adult)
      params[:adult] = 'kid-friendly'
    end

    # if the ownership filter was turned on, its value needs to be set to the
    # current user
    if params.has_key?(:own)
      params[:own] = current_user
    end

    # apply filters from the user
    @bounties = apply_filters(@bounties, params)

    respond_with @bounties.all.sort!.reverse!
  end

  # Display an individual bounty.
  def show
    if Bounty.exists?(:id => params[:id])
      @bounty = Bounty.find(params[:id])
      if !@bounty.viewable_by?(current_user)
        flash[:error] = "That bounty is private."
        if request.env['HTTP_REFERER']
          redirect_to :back
        else
          redirect_to root_url
        end
        return
      end
    else
      flash[:error] = "That bounty does not exist."
      if request.env['HTTP_REFERER']
        redirect_to :back
      else
        redirect_to root_url
      end
      return
    end
  end

  # Display the form to create a new bounty. Anyone may perform this action.
  def new
    @bounty = Bounty.new
  end

  # Create a new bounty. Anyone may perform this action.
  def create
    # If no specific artists were selected. Then create a candidacy to all
    # active artists.
    # TODO fix access pattern here
    if !params[:bounty][:artist_ids] || params[:bounty][:artist_ids] == [""]
      params[:bounty][:artist_ids] =
        Artist.available().where('id <> ?', current_user.id).pluck(:id)
    end

    @bounty = Bounty.new(bounty_create_params)
    @bounty.owner = current_user

    if @bounty.save
      flash[:notice] = "Bounty created successfully!"
      redirect_to bounty_path(@bounty)
    else
      flash[:error] = "Error saving bounty."
      render 'new'
    end
  end

  # Display the edit form for the bounty. Artists that are candidates for the
  # bounty may use this for completion of a bounty. Owners of the bounty
  # may edit all other properties.
  def edit
    if Bounty.exists?(:id => params[:id])

      @bounty = Bounty.find(params[:id])

      if !(@bounty.owner == current_user || current_user.admin?)
        flash[:error] = "You are not authorized to edit this bounty."
        if request.env['HTTP_REFERER']
          redirect_to :back
        else
          redirect_to root_url
        end
        return
      end
    else
      flash[:error] = "That bounty does not exist."
      if request.env['HTTP_REFERER']
        redirect_to :back
      else
        redirect_to root_url
      end
    end
  end

  # The complete action displays a special Edit view for the bounty, showing
  # only the artwork properties of the bounty as form fields. Once the artist
  # completes the bounty and submits the artwork, the Bounty#Update action
  # will be invoked on form submission.
  def complete
    if Bounty.exists?(:id => params[:id])

      @bounty = Bounty.find(params[:id])

      if (@bounty.acceptor_candidacy)
          if !(@bounty.acceptor_candidacy.artist == current_user)
          flash[:error] = "You are not authorized to complete this bounty."
          if request.env['HTTP_REFERER']
            redirect_to :back
          else
            redirect_to root_url
          end
          return
        end
      end
    else
      flash[:error] = "That bounty does not exist."
      if request.env['HTTP_REFERER']
        redirect_to :back
      else
        redirect_to root_url
      end
    end
  end

  # Update the bounty. This action updates the bounty model after form
  # submission from either the Edit or Complete view.
  def update
    @bounty = Bounty.find(params[:id])

    unless (@bounty.owner == current_user || @bounty.artists.include?(current_user))
      flash[:error] = "You are not authorized to edit this bounty."
      if request.env['HTTP_REFERER']
        redirect_to :back
      else
        redirect_to root_url
      end
      return
    end

    # Complete Action ----------------------------------------------------------
    # Because we know a user cannot accept his own bounty, if the current bounty
    # is accepted and the currrent user is the accepting artist, then the
    # artwork is being submitted and nothing else.
    if (@bounty.acceptor_candidacy)
      if (@bounty.acceptor_candidacy.artist == current_user)
        @bounty.completed_at = Time.now
        if @bounty.update_attributes(bounty_update_params(@bounty))
          BountyMailer.completion_email(@bounty)
          flash[:notice] = "Bounty Completed!"
          redirect_to bounty_path(@bounty)
        else
          flash[:error] = "Error completing bounty."
          logger.error "Error completing bounty #{params[:id]}: #{@bounty.inspect}; Validation errors: #{@bounty.errors.messages.inspect}"
          render 'complete'
        end
      end
    # Edit Action --------------------------------------------------------------
    else
      # If no specific artists were selected. Then create a candidacy to all
      # active artists.
      if !params[:bounty][:artist_ids] || params[:bounty][:artist_ids] == [""]
        params[:bounty][:artist_ids] =
          Artist.available().where('id <> ?', current_user.id).pluck(:id)
      end
      # Sanitize the name and description fields for bounties. Clean name of all
      # HTML, clean descs using "Relaxed" method which allows:
      #
      # "Allows an even wider variety of markup than BASIC, including images and
      # tables. Links are still limited to FTP, HTTP, HTTPS, and mailto protocols,
      # while images are limited to HTTP and HTTPS. In this mode, rel="nofollow"
      # is not added to links."
      params[:bounty][:name] = Sanitize.clean(params[:bounty][:name])
      params[:bounty][:tag_line] = Sanitize.clean(params[:bounty][:tag_line])
      params[:bounty][:description] = Sanitize.clean(params[:bounty][:description], Sanitize::Config::RELAXED)

      if @bounty.update_attributes(bounty_update_params(@bounty))
        flash[:notice] = "Bounty Updated!"
        redirect_to bounty_path(@bounty)
      else
        flash[:error] = "Error updating bounty."
        logger.error "Error updating bounty #{params[:id]}: #{@bounty.inspect}; Validation errors: #{@bounty.errors.messages.inspect}"
        render 'edit'
      end
    end
  end

  # Delete the bounty. Admins may delete any bounty. But the bounty's owner may
  # only do this if the bounty is unclaimed.
  def destroy
    @bounty = Bounty.find(params[:id])

    if (@bounty.owner == current_user && (@bounty.status == 'Unclaimed')) || current_user.admin?
      if Bounty.destroy(params[:id])
        flash[:notice] = "Bounty successfully removed!"
        redirect_to root_path
      else
        flash[:error] = "Error removing bounty."
        redirect_to root_path
      end
    else
      if @bounty.owner != current_user
        flash[:error] = "You are not authorized to remove this bounty."
        if request.env['HTTP_REFERER']
          redirect_to :back
        else
          redirect_to root_url
        end
      elsif @bounty.status == "Accepted"
        acceptor = @bounty.acceptor_candidacy.artist.get_identifier
        flash[:error] = "This bounty is being worked on by #{acceptor} and cannot be deleted."
        if request.env['HTTP_REFERER']
          redirect_to :back
        else
          redirect_to root_url
        end
      else
        flash[:error] = "This bounty is #{@bounty.status} and cannot be deleted."
        if request.env['HTTP_REFERER']
          redirect_to :back
        else
          redirect_to root_url
        end
      end
    end
  end

  private

    def bounty_create_params
      params.require(:bounty).permit(
        :name,
        :description,
        :price,
        :price_cents,
        :price_currency,
        :adult_only,
        :private,
        :complete_by,
        :tag_line,
        :mood_ids => [],
        :artist_ids => []
      )
    end

    def bounty_update_params(bounty)
      # only the acccepting artist can complete a bounty, but they can't change
      # any other bounty properties
      if bounty.acceptor_candidacy && bounty.acceptor_candidacy.artist == current_user
        return params.require(:bounty).permit(
          :preview,
          :completed_at,
          :artwork,
          :preview,
          :artwork_file_name,
          :artwork_content_type,
          :artwork_file_size,
          :artwork_updated_at,
          :preview_file_name,
          :preview_content_type,
          :preview_file_size,
          :preview_updated_at
        )
      end

      params.require(:bounty).permit(
        :name,
        :description,
        :price,
        :price_cents,
        :price_currency,
        :adult_only,
        :private,
        :complete_by,
        :tag_line,
        :mood_ids => [],
        :artist_ids => []
      )
    end
end
