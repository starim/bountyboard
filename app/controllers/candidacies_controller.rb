class CandidaciesController < ApplicationController

  # Update the the candidacy to "accept" a bounty. Only artists that have a
  # candidacy to the bounty in question may perform this action.
  def update
    if (!current_user.is_a?(Artist))
      flash[:error] = "Only artists may accept bounties."
      if request.env['HTTP_REFERER']
        redirect_to :back
      else
        redirect_to root_url
      end
      return
    end

    @candidacy = Candidacy.find(params[:id])
    if @candidacy
      if params[:candidacy][:accept] == "false"
        @candidacy.accepted_at = nil
        if @candidacy.save
          flash[:notice] = "Bounty Un-Accepted!"
          BountyMailer.unacceptance_email(@candidacy.bounty, @candidacy.artist)
        else
          flash[:error] = "Error un-accepting bounty."
        end
      else
        @candidacy.accepted_at = Time.now
        if @candidacy.save
          flash[:notice] = "Bounty Accepted!"
          BountyMailer.acceptance_email(@candidacy.bounty)
        else
          flash[:error] = "Error accepting bounty."
        end
      end
    else
      flash[:error] = "You are not a candidate for that bounty."
    end

    if @candidacy
      redirect_to bounty_path @candidacy.bounty
    else
      flash[:error] = 'Error updating candidacy'
      if request.env['HTTP_REFERER']
        redirect_to :back
      else
        redirect_to root_url
      end
    end
  end

  # Remove a candidacy between the currently logged in artist and the specified
  # bounty.
  def destroy
    @bounty = Bounty.find(params[:id])
    if (not current_user.is_a?(Artist))
      flash[:error] = "Only artists may reject bounties."
      if request.env['HTTP_REFERER']
        redirect_to :back
      else
        redirect_to root_url
      end
    end
    @candidacy = Candidacy.where(artist_id: current_user.id, bounty_id: @bounty.id).first
    unless @candidacy.nil?
      if @candidacy.destroy
        if @bounty.abandoned?
          if @bounty.destroy
            flash[:notice] = "#{@bounty.name} was completely rejected and removed!"
            redirect_to root_path
            BountyMailer.rejection_email(@bounty)
            return
          end
        end
        flash[:notice] = "Candidacy removed!"
        redirect_to root_path
      else
        flash[:error] = "Error removing candidacy."
        if request.env['HTTP_REFERER']
          redirect_to :back
        else
          redirect_to root_url
        end
      end
    else
      flash[:error] = "You are not a candidate for that bounty."
      if request.env['HTTP_REFERER']
        redirect_to :back
      else
        redirect_to root_url
      end
    end
  end

  private

    def candidacy_create_params
      params.require(:candidacy).permit(
        :bounty_id,
        :artist_id
      )
    end

    def candidacy_update_params
      params.require(:candidacy).permit()
    end

end

