class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :require_login

  private
  def require_login
    unless signed_in?
      flash[:error] = "You must sign in."
      if request.env['HTTP_REFERER']
        redirect_to :back
      else
        redirect_to root_url
      end
      return
    end
  end

  helper_method :signed_in?
  helper_method :current_user

  def signed_in?
    current_user.present?
  end

  def current_user
    if session[:user_id]
      @current_user ||=
        User.find(session[:user_id])
    else
      @current_user = nil
    end
  end
end
