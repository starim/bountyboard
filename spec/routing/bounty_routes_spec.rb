require 'spec_helper'

describe 'bounty routes' do

  it 'should map the root URL to Bounty#index' do
    expect(get: '/').to route_to(
      controller: 'bounties',
      action: 'index'
    )
  end
end
