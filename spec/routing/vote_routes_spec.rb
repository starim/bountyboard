require 'spec_helper'

describe 'vote routes' do

  it "shouldn't route vote#index" do
    expect(get: votes_path).not_to be_routable
  end

  it "shouldn't route vote#new" do
    expect(get: 'vote/new').not_to be_routable
  end

  it "shouldn't route vote#edit" do
    expect(get: 'votes/edit/1').not_to be_routable
  end
end
