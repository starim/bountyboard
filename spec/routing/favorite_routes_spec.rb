require 'spec_helper'

describe 'favorite routes' do

  it "shouldn't route favorite#index" do
    expect(get: favorites_path).not_to be_routable
  end

  it "shouldn't route favorite#new" do
    expect(get: 'favorite/new').not_to be_routable
  end

  it "shouldn't route favorite#edit" do
    expect(get: 'favorites/edit/1').not_to be_routable
  end
end
