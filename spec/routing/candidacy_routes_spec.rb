require 'spec_helper'

describe 'candidacy routes' do

  it "shouldn't route Candidacy#index" do
    expect(get: candidacies_path).not_to be_routable
  end

  it "shouldn't route Candidacy#new" do
    expect(get: 'candidacy/new').not_to be_routable
  end

  it "shouldn't route Candidacy#edit" do
    expect(get: 'candidacies/edit/1').not_to be_routable
  end
end
