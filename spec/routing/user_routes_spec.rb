require 'spec_helper'

describe 'user routes' do

  it "shouldn't route user#index" do
    expect(get: users_path).not_to be_routable
  end
end
