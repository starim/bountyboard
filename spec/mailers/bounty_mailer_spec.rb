require 'spec_helper'
require 'faker'

describe BountyMailer do
  describe 'acceptance email' do
    let(:customer) {
      FactoryGirl.create(:user, email: 'customer@example.com')
    }
    let(:accepting_artist) {
      FactoryGirl.create(:artist,
        email: 'thegrandeartiste@example.com',
        name: 'The Grande Artiste'
      )
    }
    let(:other_artist) {
      FactoryGirl.create(:artist)
    }
    let(:accepted_bounty) {
      FactoryGirl.create(:bounty,
        :accepted,
        name: 'A Grande Bounty',
        owner: customer,
        artists: [ accepting_artist ]
      )
    }
    let(:mail) {
      BountyMailer.acceptance_email(accepted_bounty)
    }

    it 'should have the correct subject line' do
      mail.subject.should == 'Your bounty has been accepted'
    end

    it 'should be sent to the correct recipient' do
      mail.to.should == [ 'customer@example.com' ]
    end

    it 'should give the bounty name somewhere in the body' do
      mail.body.encoded.should match('A Grande Bounty')
    end

    it "should identify the accepting artist in the body" do
      # if this line fails, FactoryGirl made the wrong artist the acceptor when
      # creating the bounty and the assumptions in this test will fail
      accepted_bounty.acceptor_candidacy.artist.should == accepting_artist

      mail.body.encoded.should match('The Grande Artiste')
      mail.body.encoded.should match('thegrandeartiste@example.com')
    end

    context 'for a user with a known name' do
      let(:poster) { 
        FactoryGirl.create(:user,
          name: 'Juan Carlos',
          email: 'juan@example.com'
        )
      }
      let(:accepted_bounty) {
        FactoryGirl.create(
          :bounty,
          :accepted,
          owner: poster
        )
      }
      let(:mail) {
        BountyMailer.acceptance_email(accepted_bounty)
      }

      it "should give the user's name in the to: field" do
        mail.to.should == [ 'juan@example.com' ]
        mail.to_s.should match('To: Juan Carlos <juan@example.com>')
      end
    end
  end

  describe 'un-acceptance email' do

    context 'for a valid artist' do
      let(:customer) {
        FactoryGirl.create(:user, email: 'customer@example.com')
      }
      let(:unaccepting_artist) {
        FactoryGirl.create(:artist,
          email: 'thegrandeartiste@example.com',
          name: 'The Grande Artiste'
        )
      }
      let(:unaccepted_bounty) {
        FactoryGirl.create(:bounty,
          name: 'A Grande Bounty',
          owner: customer,
          artists: [ unaccepting_artist ]
        )
      }
      let(:mail) {
        BountyMailer.unacceptance_email(unaccepted_bounty, unaccepting_artist)
      }


      it 'should have the correct subject line' do
        mail.subject.should == 'Work has stopped on your bounty'
      end

      it 'should be sent to the correct recipient' do
        mail.to.should == [ 'customer@example.com' ]
      end

      it 'should give the bounty name somewhere in the body' do
        mail.body.encoded.should match('A Grande Bounty')
      end

      it "should identify the un-accepting artist in the body" do
        mail.body.encoded.should match('The Grande Artiste')
        mail.body.encoded.should match('thegrandeartiste@example.com')
      end
    end

    it 'should object to an unaffiliated artist being named the un-acceptor' do
      unrelated_artist = FactoryGirl.create(:artist)
      unaccepted_bounty = FactoryGirl.create(:bounty)

      expect {
        BountyMailer.unacceptance_email(
          unaccepted_bounty,
          unrelated_artist
        )
      }.to raise_error
    end

    context 'for a user with a known name' do
      let(:poster) { 
        FactoryGirl.create(:user,
          name: 'Juan Carlos',
          email: 'juan@example.com'
        )
      }
      let(:unaccepted_bounty) {
        FactoryGirl.create(
          :bounty,
          owner: poster
        )
      }
      let(:mail) {
        BountyMailer.unacceptance_email(
          unaccepted_bounty,
          unaccepted_bounty.artists[0]
        )
      }

      it "should give the user's name in the to: field" do
        mail.to.should == [ 'juan@example.com' ]
        mail.to_s.should match('To: Juan Carlos <juan@example.com>')
      end
    end
  end

  describe 'completion email' do
    let(:customer) {
      FactoryGirl.create(:user, email: 'customer@example.com')
    }
    let(:finished_bounty) {
      FactoryGirl.create(:bounty,
        :completed,
        name: 'A Grande Bounty',
        owner: customer
      )
    }
    let(:mail) {
      BountyMailer.completion_email(finished_bounty)
    }

    it 'should have the correct subject line' do
      mail.subject.should == 'Your bounty is complete'
    end

    it 'should be sent to the correct recipient' do
      mail.to.should == [ 'customer@example.com' ]
    end

    it 'should give the bounty name somewhere in the body' do
      mail.body.encoded.should match(bounty_path(finished_bounty))
    end

    it 'should link to the bounty' do
    end

    context 'for a user with a known name' do
      let(:poster) { 
        FactoryGirl.create(:user,
          name: 'Juan Carlos',
          email: 'juan@example.com'
        )
      }
      let(:completed_bounty) {
        FactoryGirl.create(
          :bounty,
          :completed,
          owner: poster
        )
      }
      let(:mail) {
        BountyMailer.completion_email(completed_bounty)
      }

      it "should give the user's name in the to: field" do
        mail.to.should == [ 'juan@example.com' ]
        mail.to_s.should match('To: Juan Carlos <juan@example.com>')
      end
    end
  end

  describe 'rejection email' do
    let(:customer_email) {
      Faker::Internet.email
    }
    let(:customer) {
      FactoryGirl.create(:user, email: customer_email)
    }
    let(:rejecting_artists) {
      FactoryGirl.create_list(:artist, 5)
    }
    let(:rejected_bounty) {
      FactoryGirl.create(:bounty,
        name: 'Some Terrible Bounty That Nobody Likes',
        owner: customer,
        artists: rejecting_artists
      )
    }
    let(:mail) {
      BountyMailer.rejection_email(rejected_bounty)
    }


    it 'should have the correct subject line' do
      mail.subject.should == 'Your bounty has been rejected'
    end

    it 'should be sent to the correct recipient' do
      mail.to.should == [ customer_email ]
    end

    it 'should give the bounty name somewhere in the body' do
      mail.body.encoded.should match('Some Terrible Bounty That Nobody Likes')
    end
  end

end
