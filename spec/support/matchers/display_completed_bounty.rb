# a custom matcher that checks that the specified completed bounty is shown
# in a bounty square display, used like so:
# page.should display_completed_bounty id
RSpec::Matchers.define :display_completed_bounty do |id|
  match do |page|
    begin
      page.should have_selector(
        ".bounty-square a[href='/bounties/#{id}']"
      )
      true
    rescue
      false
    end
  end
end

