# a custom matcher that checks that the specified number of bounties are
# displayed, used like so:
# page.should display_this_many_bounties 5
RSpec::Matchers.define :display_this_many_bounties do |expected|
  match do |page|
    begin
      page.should have_selector '.bounty-square', :count => expected
      true
    rescue
      false
    end
  end
end

