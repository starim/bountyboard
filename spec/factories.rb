include VotesHelper

FactoryGirl.define do

  factory :user, :aliases => [:owner] do
    sequence(:email) { |n| "user#{n}@example.com" }

    ignore do
      comment_count 0
      comment_factory { lambda {|user, comment_count|
        FactoryGirl.build_list(:comment, comment_count, :user => user)
      }}
    end

    trait :admin do
      admin true
    end

    after :build do |user, evaluator|
      if user.comments.length == 0
        user.comments =
          evaluator.comment_factory.call(user, evaluator.comment_count)
      end
    end
  end

  factory :artist, :class => :artist, :parent => :user do
    sequence(:email) { |n| "artist#{n}@example.com" }
    sequence(:name) { |n| "artist#{n}" }
    bio "This is my artist's biography."
    bounty_rules "These are the rules for my bounties."
    approved true
  end

  factory :mood do
    sequence(:name) { |n| "mood#{n}" }
  end

  factory :favorite do
    user
    bounty
  end

  factory :vote do
    user
    bounty

    trait :upvote do
      vote_type VoteType::UPVOTE
    end

    trait :downvote do
      vote_type VoteType::DOWNVOTE
    end
  end

  factory :bounty do
    sequence(:name) { |n| "bounty#{n}" }
    tag_line "Bounty Tag Line."
    description "Bounty description."
    price Bounty.MINIMUM_PRICE
    owner

    ignore do
      artists nil
      artist_count 1
      artist_factory { lambda {|bounty, artist_count|
        if artists
          Array(artists)
        else
          FactoryGirl.build_list(:artist, artist_count)
        end
      }}
      moods nil
      mood_count 1
      mood_factory { lambda {|bounty, mood_count|
        if moods
          Array(moods)
        else
          FactoryGirl.build_list(:mood, mood_count)
        end
      }}
      votes nil
      vote_count 0
      vote_factory { lambda {|bounty, vote_count|
        if votes
          Array(votes)
        else
          FactoryGirl.build_list(:vote, vote_count, :bounty => bounty)
        end
      }}
      comments nil
      comment_count 0
      comment_factory { lambda {|bounty, comment_count|
        if comments
          Array(comments)
        else
          FactoryGirl.build_list(:comment, comment_count, :bounty => bounty)
        end
      }}
    end

    after :build do |bounty, evaluator|
      if bounty.artists.length == 0
        bounty.artists =
          evaluator.artist_factory.call(bounty, evaluator.artist_count)
      end
      if bounty.moods.length == 0
        bounty.moods =
          evaluator.mood_factory.call(bounty, evaluator.mood_count)
      end
      if bounty.votes.length == 0
        bounty.votes =
          evaluator.vote_factory.call(bounty, evaluator.vote_count)
      end
      if bounty.comments.length == 0
        bounty.comments =
          evaluator.comment_factory.call(bounty, evaluator.comment_count)
      end
    end

    trait :private do
      private true
    end

    trait :accepted do
      after :create do |bounty|
        if !bounty.candidacies.first.accepted_at
          bounty.candidacies.first.accepted_at = DateTime.now
          bounty.candidacies.first.save!
        end
      end
    end

    trait :completed do
      after :create do |bounty|
        if !bounty.candidacies.first.accepted_at
          bounty.candidacies.first.accepted_at = DateTime.now
          bounty.candidacies.first.save!
        end
        bounty.completed_at = DateTime.now
        bounty.artwork_file_name = 'test.png'
        bounty.save!
      end
    end
  end

  factory :personality do
    bounty
    mood

    trait :acceptor do
      accepted_on { DateTime.now }
    end
  end

  factory :candidacy do
    bounty
    artist

    trait :acceptor do
      accepted_at { DateTime.now }
    end
  end

  factory :comment do
    user
    bounty
    content "This is a comment on a bounty."
  end

  factory :conversation do
    ignore do
      conversation_participation_count 2
    end

    after :build do |conversation, evaluator|
      if(
        conversation.conversation_participations.length == 0
      )
        conversation.conversation_participations = FactoryGirl.build_list(
          :conversation_participation,
          evaluator.conversation_participation_count,
          conversation: conversation
        )
      end
    end
  end

  factory :conversation_participation do

    conversation
    user

    ignore do
      message_count 0
    end

    after :build do |conversation_participation, evaluator|
      if(
        conversation_participation.messages.length == 0
      )
        conversation_participation.messages = FactoryGirl.build_list(
          :message,
          evaluator.message_count,
          conversation_participation: conversation_participation
        )
      end
    end
  end

  factory :message do
    conversation_participation
    content "This is a message to another user in the system."
  end
end

