require 'spec_helper'

describe 'Bounty#index page' do

  context 'when there is only one open bounty' do

    before {
      FactoryGirl.create(
        :bounty,
        :name => 'My Special Bounty',
        :tag_line => 'Tag line for my special bounty.',
        :description => 'There is one bounty, and it is mine!',
        :price => 14.99
      )
      visit root_path
    }

    it 'should display one bounty' do
      page.should display_this_many_bounties 1
    end

    it "should show the bounty's name and tag line, but not the description" do
      page.should have_selector(
        '.bounty-square',
        :text => 'My Special Bounty'
      )
      page.should have_selector(
        '.bounty-square',
        :text => 'Tag line for my special bounty.'
      )
      page.should_not have_selector(
        '.bounty-square',
        :text => 'There is one bounty, and it is mine!'
      )
    end
  end

  context 'when there are multiple bounties' do

      before {
        FactoryGirl.create_list :bounty, num_bounties

        visit root_path
      }

    context 'that all fit on the page' do
      let(:num_bounties) { BountiesController.BOUNTIES_PER_PAGE - 1 }

      it 'should display every bounty' do
        page.should display_this_many_bounties num_bounties
      end
    end

    context "that don't fit on the page" do
      let(:num_bounties) { BountiesController.BOUNTIES_PER_PAGE + 2 }

      it 'should paginate the bounties' do
        page.should display_this_many_bounties BountiesController.BOUNTIES_PER_PAGE
        click_link 'Next'
        page.should display_this_many_bounties 2
      end
    end
  end

  describe 'bounty square status stamp' do

    # since there's no stamp associated with unclaimed bounties, we only test
    # the accepted and completed statuses for stamps
    [ 'accepted', 'completed' ].each do |status|
      context "on a #{status} bounty" do

        before do
          FactoryGirl.create(:bounty, status.to_sym)
          visit root_path
        end

        it "should read \"#{status}\"" do
          page.should have_selector ".#{status}-stamp"
        end
      end
    end
  end
end

