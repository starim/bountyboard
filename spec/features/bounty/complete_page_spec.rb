require 'spec_helper'

describe 'Bounty#complete page' do

  let!(:customer) { FactoryGirl.create :user }
  let!(:correct_artist) { FactoryGirl.create :artist, :name => 'Correct Artist' }
  let!(:wrong_artist) { FactoryGirl.create :artist, :name => 'Wrong Artist' }

  let!(:hopeful_mood) { FactoryGirl.create :mood, :name => 'Hope' }
  let!(:joyful_mood) { FactoryGirl.create :mood, :name => 'Joy' }

  let!(:existing_bounty) {
    FactoryGirl.create(
      :bounty,
      :owner => customer,
      :name => 'Original Name',
      :tag_line => 'Original Tag Line',
      :description => 'Original Description',
      :price => 25.18,
      :moods => [ hopeful_mood, joyful_mood ],
      :artists => [ correct_artist ]
    )
  }


  before do
    log_in correct_artist
    visit bounty_path existing_bounty
    click_link 'Accept'
    click_link 'Complete'
  end

  it 'should redirect to root route when bounty does not exist' do
    visit bounty_path existing_bounty.id+1
    current_path.should == root_path
    page.should have_content 'That bounty does not exist.'
  end

  it 'should have a "Back" button that navigates to the bounty show page' do
    # TODO: if we ever enable Javascript in tests, change this test
    # to actually click the button check that is has actually gone
    # back a page
    page.should have_link('Back')
  end

  context 'after submitting valid artwork' do

    before do
      within("#form-area") do
        attach_file 'Artwork', "#{Rails.root}/spec/images/Cheese.jpg"
        click_button('Update Bounty')
      end
    end

    it "should redirect to the bounty's show page" do
      current_path.should == bounty_path(existing_bounty)
      page.should have_content 'Bounty Completed!'
    end
  end

  context 'after submitting valid artwork with a preview image' do

    before do
      within("#form-area") do
        attach_file 'Artwork', "#{Rails.root}/spec/images/Cheese.jpg"
        attach_file 'Preview', "#{Rails.root}/spec/images/CheeseThumb.jpg"
        click_button('Update Bounty')
      end
    end

    it "should redirect to the bounty's show page" do
      current_path.should == bounty_path(existing_bounty)
      page.should have_content 'Bounty Completed!'
    end
  end

  context 'after submitting just a preview image' do

    before do
      within("#form-area") do
        attach_file 'Preview', "#{Rails.root}/spec/images/CheeseThumb.jpg"
        click_button('Update Bounty')
      end
    end

    it "should redirect to the bounty's show page" do
      current_path.should == bounty_path(existing_bounty)
      page.should have_content 'Error completing bounty.'
    end
  end

  context 'after submitting invalid artwork' do

    before do
      within("#form-area") do
        attach_file 'Artwork', "#{Rails.root}/spec/images/FakeImage.php"
        attach_file 'Preview', "#{Rails.root}/spec/images/CheeseThumb.jpg"
        click_button('Update Bounty')
      end
    end

    it "should redirect to the bounty's show page" do
      current_path.should == bounty_path(existing_bounty)
      page.should have_content 'Error completing bounty.'
    end
  end

end
