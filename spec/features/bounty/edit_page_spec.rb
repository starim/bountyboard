require 'spec_helper'

describe 'Bounty#edit page' do

  let!(:customer) { FactoryGirl.create :user }
  let!(:bold_artist) { FactoryGirl.create :artist, :name => 'Bold Artist' }
  let!(:lazy_artist) { FactoryGirl.create :artist, :name => 'Lazy Artist' }
  let!(:unpopular_artist) { FactoryGirl.create :artist, :name => 'Unpopular Artist' }

  let!(:graceful_mood) { FactoryGirl.create :mood, :name => 'Grace' }
  let!(:hopeful_mood) { FactoryGirl.create :mood, :name => 'Hope' }
  let!(:joyful_mood) { FactoryGirl.create :mood, :name => 'Joy' }

  let!(:existing_bounty) {
    FactoryGirl.create(
      :bounty,
      :owner => customer,
      :name => 'Original Name',
      :tag_line => 'Original Tag Line',
      :description => 'Original Description',
      :price => 25.18,
      :moods => [ hopeful_mood, joyful_mood ],
      :artists => [ bold_artist, lazy_artist ]
    )
  }

  before do
    log_in customer
    visit bounty_path existing_bounty
    click_link 'Update'
  end

  it 'should redirect to root route when bounty does not exist' do
    visit bounty_path existing_bounty.id+1
    page.should have_content 'That bounty does not exist.'
  end


  it 'should have a working "Back" button for easy navigation' do
    # TODO: if we ever enable Javascript in tests, change this test
    # to actually click the button check that is has actually gone
    # back a page
    page.should have_link('Back')
  end

  {
    'Name' => 'Original Name',
    'Tag Line' => 'Original Tag Line',
    'Description' => 'Original Description',
    'Price' => '25.18'
  }.each do |field_name, bounty_value|
    it "should fill in the form's #{field_name} field wth the bounty's data" do
      within('#bounties-edit') do
        find_field(field_name).value.should == bounty_value
      end
    end
  end
  # test moods and authors are more complex since they're checkboxes, so they
  # get their own specialized tests
  it "should fill in the form's Artists checkboxes wth the bounty's artists" do
    within('#bounties-edit') do
      find_field('Bold Artist').should be_checked
      find_field('Lazy Artist').should be_checked
      find_field('Unpopular Artist').should_not be_checked
    end
  end
  it "should fill in the form's Moods checkboxes wth the bounty's moods" do
    within('#bounties-edit') do
      find_field('Hope').should be_checked
      find_field('Joy').should be_checked
      find_field('Grace').should_not be_checked
    end
  end

  context 'after submitting valid bounty information' do

    before do
      within("#bounties-edit") do
        fill_in 'Name', :with => 'New Name'
        fill_in 'Tag Line', :with => 'New Tag Line'
        fill_in 'Description', :with => 'New Description'
        fill_in 'Price', :with => '35.27'
        uncheck 'Lazy Artist'
        uncheck 'Joy'
        click_button('Update Bounty')
      end
    end

    it "should redirect to the bounty's show page" do
      current_path.should == bounty_path(existing_bounty)
      page.should have_content 'Bounty Updated'
    end

    describe 'the updated bounty' do
      it 'should reflect the updated information' do
        within '#bounties-show' do
          page.should have_content 'New Name'
          page.should_not have_content 'Original Name'

          page.should have_content 'New Description'
          page.should_not have_content 'Original Description'

          # the show page doesn't display the tag line, so we'll have to cheat
          # and check the bounty model directly
          Bounty.first.tag_line.should == 'New Tag Line'

          page.should have_content '$35.27'
          page.should_not have_content '$25.18'

          page.should have_content 'Bold Artist'
          page.should_not have_content 'Lazy Artist'

          page.should have_content 'Hope'
          page.should_not have_content 'Joy'
        end
      end
    end
  end

  context 'after submitting a blank value' do

    shared_examples_for "check bounty hasn't been updated" do
      subject { Bounty.first }
      its(:name) { should == 'Original Name' }
      its(:tag_line) { should == 'Original Tag Line' }
      its(:description) { should == 'Original Description' }
      its(:price) { should == 25.18 }
      its(:moods) { should == [ hopeful_mood, joyful_mood ] }
      its(:artists) { should == [ bold_artist, lazy_artist ] }
    end

    # fills in all fields in the bounty update form except the one specified
    # by field_to_skip with new values
    def fill_in_all_but(field_to_skip)
      within('#bounties-edit') do
        if field_to_skip == 'Name'
          fill_in 'Name', :with => ''
        else
          fill_in 'Name', :with => 'New Name'
        end
        if field_to_skip == 'Tag line'
          fill_in 'Tag Line', :with => ''
        else
          fill_in 'Tag Line', :with => 'New Tag Line'
        end
        if field_to_skip == 'Description'
          fill_in 'Description', :with => ''
        else
          fill_in 'Description', :with => 'New Description'
        end
        if field_to_skip == 'Price'
          fill_in 'Price', :with => ''
        else
          fill_in 'Price', :with => '35.27'
        end

        uncheck 'Lazy Artist'
        uncheck 'Unpopular Artist'
        if field_to_skip == 'Artists'
          uncheck 'Bold Artist'
        end

        uncheck 'Joy'
        if field_to_skip == 'Moods'
          uncheck 'Hope'
        end
      end
    end

    [ 'Name', 'Tag line', 'Description', 'Price', 'Moods' ].each do |field|
      context "for the bounty's #{field}" do
        before do
          fill_in_all_but(field)
          click_button('Update Bounty')
        end

        include_examples "check bounty hasn't been updated"

        it "should inform the user of an error with the new #{field}" do
          page.should have_selector '#error-wrapper', :text => field
        end
      end
    end

    context "for the bounty's candidate artists" do
      before do
        fill_in_all_but('Artists')
        click_button('Update Bounty')
      end

      it 'should update the bounty with all artists as candidates' do
        Bounty.first.artists.should match_array([
          bold_artist,
          lazy_artist,
          unpopular_artist
        ])
      end
    end
  end

  context 'when there are inactive and unapproved artists' do

    let!(:unapproved_artist_artist) {
      FactoryGirl.create(
        :artist,
        :name => 'Unapproved Artist',
        :approved => false
      )
    }
    let!(:inactive_artist) {
      FactoryGirl.create(
        :artist,
        :name => 'Inactive Artist'
      )
    }

    before do
      inactive_artist.destroy
      visit bounty_path existing_bounty
      click_link 'Update'
    end

    it 'should not display invalid artists as possible candidates' do
      within('#bounties-edit') do
        page.should_not have_content 'Unapproved Artist'
        page.should_not have_content 'Inactive Artist'
      end
    end

    context 'and the user submits the form with no artists specified' do

      before do
        uncheck 'Bold Artist'
        uncheck 'Lazy Artist'
        uncheck 'Unpopular Artist'

        click_button 'Update Bounty'
      end

      it 'should not add inactive and unapproved artists as candidates' do
        new_candidate_artists = Bounty.where( :name => 'Original Name' ).first.artists
        new_candidate_artists.should match_array(
          [
            bold_artist,
            lazy_artist,
            unpopular_artist
          ]
        )
      end
    end
  end

  context 'when updating bounty information with unusual Unicode characters' do

    before do
      within("#bounties-edit") do
        fill_in 'Name', :with => 'ಠ__ಠ'
        fill_in 'Tag Line', :with => '(╯°□°）╯︵ ┻━┻)'
        fill_in 'Description', :with => <<NYAN
~~~ ╔͎═͓═͙╗
~~~ ╚̨̈́═̈́﴾ ̥̂˖̫˖̥  ̂ )
NYAN

        click_button('Update Bounty')
      end
    end

    it 'should show the text without mangling it' do
      within('#bounties-show') do
        page.should have_content 'ಠ__ಠ'
        page.should have_content <<NYAN
~~~ ╔͎═͓═͙╗
~~~ ╚̨̈́═̈́﴾ ̥̂˖̫˖̥  ̂ )
NYAN

      end

      visit root_path
      page.should have_content '(╯°□°）╯︵ ┻━┻)'
    end
  end

  context 'when submitting bounty information containing dangerous HTML' do

    [ 'Name', 'Tag Line', 'Description' ].each do |field|

      context "for the #{field} field" do
        before do
          within('#bounties-edit') do
            fill_in field, :with => <<DANGEROUS_CONTENT
This stuff is dangerous!
<style type="text/css"></style>
<script type="text/javascript></script>
<div id="dangerous_div" style="height: 10000px; width: 10000px"></div>
DANGEROUS_CONTENT

            click_button('Update Bounty')
          end
        end

        if field == 'Tag Line'
          it 'should sanitize the dangerous content' do
            visit root_path
            page.should_not have_selector '.bounty-square script'
            page.should_not have_selector '.bounty-square style'
            page.should_not have_selector '#dangerous_div'
          end
        else
          it 'should sanitize the dangerous content' do
            within('#bounties-show') do
              page.should_not have_selector 'script'
              page.should_not have_selector 'style'
              page.should_not have_selector '#dangerous_div'
            end
          end
        end
      end
    end
  end

  context 'when an artist updates a bounty they own' do

    let(:customer_artist) {
      FactoryGirl.create(
        :artist,
        :name => 'Artist who wants some art.'
      )
    }

    let(:artist_owned_bounty) {
      FactoryGirl.create(
        :bounty,
        :owner => customer_artist
      )
    }

    before do
      log_in customer_artist
      visit bounty_path(artist_owned_bounty)
      click_link 'Update'
    end

    it "shouldn't show that same artist as a possible candidate" do
      within('#bounty_artists_input') do
        page.should_not have_content 'Artist who wants some art.'
      end
    end
  end
end
