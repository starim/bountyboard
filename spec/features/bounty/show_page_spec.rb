require 'spec_helper'

describe 'Bounty#show page' do

  let(:owner) { FactoryGirl.create :user }
  let(:artist) { FactoryGirl.create :artist, :name => "d'Artagnan" }
  let(:other_artist) { FactoryGirl.create :artist }

  context 'for an unclaimed bounty' do
    let(:unclaimed_bounty) {
      FactoryGirl.create(
        :bounty,
        :owner => owner,
        :name => 'An Average Bounty',
        :tag_line => 'Tag line for this average bounty.',
        :description => 'This is a pretty normal bounty.',
        :moods => [
          FactoryGirl.create(:mood, :name => 'Peaceful Mood'),
          FactoryGirl.create(:mood, :name => 'Warm Mood')
        ],
        :complete_by => DateTime.strptime(
          '2018-08-22 Pacific Time (US & Canada)',
          '%Y-%m-%d %Z'
        ),
        :artists => [
          artist,
          FactoryGirl.create(:artist, :name => 'Athos'),
          FactoryGirl.create(:artist, :name => 'Porthos'),
          FactoryGirl.create(:artist, :name => 'Aramis')
        ]
      )
    }

    before do
      log_in user
      visit bounty_path unclaimed_bounty
    end

    shared_examples_for 'unclaimed bounty display' do
      |
        owner_controls_expected: false,
        acceptance_controls_expected: false
      |

      it 'should redirect to root route when bounty does not exist' do
        visit bounty_path unclaimed_bounty.id+1
        page.should have_content 'That bounty does not exist.'
      end

      it "should display the bounty's name" do
        page.should have_selector(
          '#bounties-show',
          :text => 'An Average Bounty'
        )
      end

      it "should display the bounty's description" do
        page.should have_selector(
          '#bounties-show',
          :text => 'This is a pretty normal bounty.'
        )
      end

      it "shouldn't display the bounty's tag line" do
        page.should_not have_selector(
          '#bounties-show',
          :text => 'Tag line for this average bounty.'
        )
      end

      it "should list the bounty's moods" do
        page.should have_selector '#bounties-show', :text => 'Peaceful Mood'
        page.should have_selector '#bounties-show', :text => 'Warm Mood'
      end

      it "should display the bounty's due date" do
        page.should have_selector '#bounties-show', :text => 'August 22, 2018'
      end

      it 'should have a "Back" button that navigates to the index page' do
        # TODO: if we ever enable Javascript in tests, change this
        # test to actually click the button check that is has
        # actually gone back a page
        page.should have_link('Back')
      end

      it "should display the bounty's candidacies" do
        page.should have_selector '#bounties-show', :text => "d'Artagnan"
        page.should have_selector '#bounties-show', :text => 'Athos'
        page.should have_selector '#bounties-show', :text => 'Porthos'
        page.should have_selector '#bounties-show', :text => 'Aramis'
      end

      if owner_controls_expected
        it "should display owner controls" do
          page.should have_selector '.bounty-button', :text => 'Update'
          page.should have_selector '.bounty-button', :text => 'Delete'
        end
      else
        it "shouldn't display owner controls" do
          page.should_not have_selector '.bounty-button', :text => 'Update'
          page.should_not have_selector '.bounty-button', :text => 'Delete'
        end
      end

      if acceptance_controls_expected
        it "should display artist acceptance controls" do
          page.should have_selector '.bounty-button', :text => 'Accept'
          page.should have_selector '.bounty-button', :text => 'Reject'
        end
      else
        it "shouldn't display artist acceptance controls" do
          page.should_not have_selector '.bounty-button', :text => 'Accept'
          page.should_not have_selector '.bounty-button', :text => 'Reject'
        end
      end
    end

    context 'when a guest visits' do
      let(:user) { nil }

      include_examples 'unclaimed bounty display'
    end

    context 'when a generic user visits' do
      let(:user) { FactoryGirl.create :user }

      include_examples 'unclaimed bounty display'
    end

    context 'when the bounty owner visits' do
      let(:user) { owner }

      include_examples 'unclaimed bounty display', owner_controls_expected: true
    end

    context 'when a candidate artist visits' do
      let(:user) { artist }

      include_examples 'unclaimed bounty display', acceptance_controls_expected: true
    end

    context 'when a non-candidate artist visits' do
      let(:user) { other_artist }

      include_examples 'unclaimed bounty display'
    end
  end

  context 'for an accepted bounty' do
    let(:accepted_bounty) {
      FactoryGirl.create(
        :bounty,
        :accepted,
        :owner => owner,
        :name => 'An Average Claimed Bounty',
        :tag_line => 'Tag line for this claimed bounty.',
        :description => 'This is a pretty normal claimed bounty.',
        :moods => [
          FactoryGirl.create(:mood, :name => 'Peaceful Mood'),
          FactoryGirl.create(:mood, :name => 'Warm Mood')
        ],
        :complete_by => DateTime.strptime(
          '2018-08-22 Pacific Time (US & Canada)',
          '%Y-%m-%d %Z'
        ),
        :artists => [
          artist,
          FactoryGirl.create(:artist, :name => 'Athos'),
          FactoryGirl.create(:artist, :name => 'Porthos'),
          FactoryGirl.create(:artist, :name => 'Aramis')
        ]
      )
    }

    before do
      log_in user
      visit bounty_path accepted_bounty
    end

    shared_examples_for 'accepted bounty display' do
      | completion_controls_expected: false |

      it "should display the bounty's name" do
        page.should have_selector(
          '#bounties-show',
          :text => 'An Average Claimed Bounty'
        )
      end

      it "should display the bounty's description" do
        page.should have_selector(
          '#bounties-show',
          :text => 'This is a pretty normal claimed bounty.'
        )
      end

      it "shouldn't display the bounty's tag line" do
        page.should_not have_selector(
          '#bounties-show',
          :text => 'Tag line for this claimed bounty.'
        )
      end

      it "should list the bounty's moods" do
        page.should have_selector '#bounties-show', :text => 'Peaceful Mood'
        page.should have_selector '#bounties-show', :text => 'Warm Mood'
      end

      it "should display the bounty's due date" do
        page.should have_selector '#bounties-show', :text => 'August 22, 2018'
      end

      it 'should have a "Back" button that navigates to the index page' do
        # TODO: if we ever enable Javascript in tests, change this
        # test to actually click the button check that is has
        # actually gone back a page
        page.should have_link('Back')
      end

      it "should only display the accepting artist" do
        page.should have_selector '#bounties-show', :text => "d'Artagnan"
        page.should_not have_selector '#bounties-show', :text => 'Athos'
        page.should_not have_selector '#bounties-show', :text => 'Porthos'
        page.should_not have_selector '#bounties-show', :text => 'Aramis'
      end

      it "shouldn't display owner controls" do
        page.should_not have_selector '.bounty-button', :text => 'Update'
        page.should_not have_selector '.bounty-button', :text => 'Delete'
      end

      it "shouldn't display artist acceptance controls" do
        # due to one of the buttons having text 'Un-Accept', we have to force
        # this selector to match against the text 'Accept' exactly, and the
        # only way to do that with a Capybara CSS selector is to use a regex
        page.should_not have_selector '.bounty-button', :text => '/\AAccept\z/'
        page.should_not have_selector '.bounty-button', :text => 'Reject'
      end

      if completion_controls_expected
        it "should display artist completion controls" do
          page.should have_selector '.bounty-button', :text => 'Un-Accept'
          page.should have_selector '.bounty-button', :text => 'Complete'
        end
      else
        it "shouldn't display artist completion controls" do
          page.should_not have_selector '.bounty-button', :text => 'Complete'
        end
      end
    end

    context 'when a guest visits' do
      let(:user) { nil }

      include_examples 'accepted bounty display'
    end

    context 'when a generic user visits' do
      let(:user) { FactoryGirl.create :user }

      include_examples 'accepted bounty display'
    end

    context 'when the bounty owner visits' do
      let(:user) { owner }

      include_examples 'accepted bounty display'
    end

    context 'when a candidate artist visits' do
      let(:user) { artist }

      include_examples 'accepted bounty display', completion_controls_expected: true
    end

    context 'when a non-candidate artist visits' do
      let(:user) { other_artist }

      include_examples 'accepted bounty display'
    end
  end

  context 'for a completed bounty' do
    let(:completed_bounty) {
      FactoryGirl.create(
        :bounty,
        :completed,
        :created_at => DateTime.strptime(
          '2014-02-21 Pacific Time (US & Canada)',
          '%Y-%m-%d %Z'
        ),
        :name => 'Fine Art',
        :tag_line => 'Complete my bounty, please.',
        :description => 'I would like something really elaborate.',
        :price => 943.67,
        :artists => [ artist ]
      )
    }

    before do
      # set the acceptance and completion dates here, since they
      # can't be easily set in the factory that creates bounties
      completed_bounty.completed_at = DateTime.strptime(
          '2011-08-22 Pacific Time (US & Canada)',
          '%Y-%m-%d %Z'
      )
      completed_bounty.save!
      candidacy = completed_bounty.acceptor_candidacy
      candidacy.accepted_at = DateTime.strptime(
          '2009-02-02 Pacific Time (US & Canada)',
          '%Y-%m-%d %Z'
      )
      candidacy.save!

      log_in FactoryGirl.create(:user)
      visit bounty_path(completed_bounty)
    end

    it "should display the accepting artist's name" do
      within('#bounties-show') do
        page.should have_content(
          completed_bounty.acceptor_candidacy.artist.name
        )
      end
    end

    it 'should display the date the bounty was accepted' do
      within('#bounties-show') do
        page.should have_content(
          completed_bounty.acceptor_candidacy.accepted_at.strftime("%B %d, %Y")
        )
      end
    end

    {
      'name' => 'Fine Art',
      'price' => '$943.67',
      'created_at' => 'February 21, 2014',
      'completed_at' => 'August 22, 2011'
    }.each do |field, expected_text|
      it "should display the bounty's #{field}" do
        within('#bounties-show') do
          page.should have_content(expected_text)
        end
      end
    end

    {
      'tag line' => 'Complete my bounty, please.',
      'description' => 'I would like something really elaborate.'
    }.each do |field, expected_text|
      it "should not display the bounty's #{field}" do
        within('#bounties-show') do
          page.should_not have_content(expected_text)
        end
      end
    end

    it 'should display the completed artwork' do
      page.should have_selector '#bounties-show .completed-art'
      page.find('.completed-art')['src'].should have_content('test.png')
    end
  end

  context 'when the bounty has advanced Markdown in its description' do

    let(:markdown_bounty) {
      FactoryGirl.create(
        :bounty,
        :description => <<MARKDOWN
1. asdf
2. *asdf*
3. **asdf**
MARKDOWN
      )
    }

    before { visit bounty_path(markdown_bounty) }

    it 'should display the Markdown converted to HTML' do
      within('#bounties-show .bounty-desc') do
        page.should have_selector 'ol li', :count => 3
        page.should have_selector 'li em'
        page.should have_selector 'li strong'
      end
    end
  end

  context 'when the bounty has links in its description' do

    let(:bounty_with_link) {
      FactoryGirl.create(
        :bounty,
        :description => <<MARKDOWN
Here is a description [with a link](http://www.example.com).
MARKDOWN
      )
    }

    before { visit bounty_path(bounty_with_link) }

    it 'should add the "nofollow" attribute to those links' do
      page.should have_selector '.bounty-desc a[rel="nofollow"]'
    end
  end
end
