require 'spec_helper'

describe 'Bounty#create page' do

  # This shared context logs a user in, navigates to the bounty creation form,
  # and fills it in form with valid values but does not submit it.
  shared_context 'bounty create form filled in' do

    let!(:customer) { FactoryGirl.create :user }
    let!(:bold_artist) { FactoryGirl.create :artist, :name => 'Bold Artist' }

    let!(:graceful_mood) { FactoryGirl.create :mood, :name => 'Grace' }

    before do
      log_in customer
      visit new_bounty_path

      within("#bounties-new") do
        fill_in 'Name', :with => 'A Test Bounty'
        fill_in 'Tag Line', :with => 'I would like this test to work...'
        fill_in 'Description', :with => <<DESC
...so I can go jogging.
I like jogging.
DESC
        fill_in 'Price', :with => '25.18'

        check 'Grace'
      end
    end
  end

  describe 'layout' do
    include_context 'bounty create form filled in'

    it 'should have a "Back" button that navigates to the index page' do
      # TODO: if we ever enable Javascript in tests, change this test
      # to actually click the button check that is has actually gone
      # back a page
      page.should have_link('Back')
    end
  end

  context 'when submitting valid bounty information' do

    let!(:lazy_artist) { FactoryGirl.create :artist, :name => 'Lazy Artist' }
    let!(:unpopular_artist) { FactoryGirl.create :artist, :name => 'Unpopular Artist' }

    let!(:hopeful_mood) { FactoryGirl.create :mood, :name => 'Hope' }
    let!(:joyful_mood) { FactoryGirl.create :mood, :name => 'Joy' }

    include_context 'bounty create form filled in'

    before do
      within("#bounties-new") do
        # all the artist checkboxes are checked by default, so reset them to
        # be unchecked
        uncheck 'Bold Artist'
        uncheck 'Lazy Artist'
        uncheck 'Unpopular Artist'

        check 'Bold Artist'
        check 'Lazy Artist'

        uncheck 'Grace'
        check 'Hope'
        check 'Joy'

        click_button('Create Bounty')
      end
    end

    it 'should create the bounty in the database' do
      Bounty.count.should == 1
    end

    it 'should redirect to the bounty#show page' do
      current_path.should == bounty_path(Bounty.first)
    end

    it 'should show the bounty with the submitted information' do
      page.should have_content 'Bounty created successfully'
      within('#bounties-show') do
        page.should have_content 'A Test Bounty'
        page.should have_content <<DESC
...so I can go jogging.
I like jogging.
DESC
        page.should have_content '$25.18'
        page.should have_content 'Bold Artist'
        page.should have_content 'Lazy Artist'
        page.should have_content 'Hope'
        page.should have_content 'Joy'
      end
    end

    describe 'the new bounty' do
      subject { Bounty.find_by_name('A Test Bounty') }
      its(:name) { should == 'A Test Bounty' }
      its(:tag_line) { should == 'I would like this test to work...' }
      its(:description) { should == "...so I can go jogging.\r\nI like jogging.\r\n"  }
      its(:price) { should == 25.18 }
      its(:artists) { should match_array([ bold_artist, lazy_artist ]) }
      its(:moods) { should match_array([ hopeful_mood, joyful_mood ]) }
    end
  end

  context 'when submitting a blank value' do

    let!(:lazy_artist) { FactoryGirl.create :artist, :name => 'Lazy Artist' }
    let!(:unpopular_artist) { FactoryGirl.create :artist, :name => 'Unpopular Artist' }

    include_context 'bounty create form filled in'

    [ 'Name', 'Tag Line', 'Description', 'Price', 'Moods' ].each do |field|
      context "for the bounty's #{field}" do
        before do
          if field == 'Moods'
            uncheck 'Grace'
          else
            fill_in field, :with => ''
          end
          click_button('Create Bounty')
        end

        it 'should not create the invalid bounty' do
          Bounty.count.should == 0
        end

        it "should inform the user of an error with the #{field}" do
          page.should have_selector '#error-wrapper', :text => field.capitalize
        end
      end
    end

    context "for the bounty's candidate artists" do
      before do
        uncheck 'Bold Artist'
        uncheck 'Lazy Artist'
        uncheck 'Unpopular Artist'

        click_button('Create Bounty')
      end

      it 'should create the bounty with all available artists as candidates' do
        Bounty.count.should == 1
        Bounty.first.artists.should match_array([
          bold_artist,
          lazy_artist,
          unpopular_artist
        ])
      end
    end
  end

  context 'when there are inactive and unapproved artists' do

    let!(:unapproved_artist_artist) {
      FactoryGirl.create(
        :artist,
        :name => 'Unapproved Artist',
        :approved => false
      )
    }
    let!(:inactive_artist) {
      FactoryGirl.create(
        :artist,
        :name => 'Inactive Artist'
      )
    }

    before {
      inactive_artist.destroy
      log_in FactoryGirl.create(:user)
      visit new_bounty_path
    }

    it 'should not display invalid artists as possible candidates' do
      within('#bounties-new') do
        page.should_not have_content 'Unapproved Artist'
        page.should_not have_content 'Inactive Artist'
      end
    end

    context 'and the user submits the form with no artists specified' do

      include_context 'bounty create form filled in'

      before do
        uncheck 'Bold Artist'
        click_button 'Create'
      end

      it 'should not add inactive and unapproved artists as candidates' do
        Bounty.count.should == 1
        Bounty.first.artists.should match_array( [ bold_artist ])
      end
    end
  end

  context 'when submitting bounty information with unusual Unicode characters' do

    include_context 'bounty create form filled in'

    before do
      within('#bounties-new') do
        fill_in 'Name', :with => 'ಠ__ಠ'
        fill_in 'Tag Line', :with => '(╯°□°）╯︵ ┻━┻)'
        fill_in 'Description', :with => <<NYAN
~~~ ╔͎═͓═͙╗
~~~ ╚̨̈́═̈́﴾ ̥̂˖̫˖̥  ̂ )
NYAN

        click_button('Create Bounty')
      end
    end

    it 'should show the text without mangling it' do
      within('#bounties-show') do
        page.should have_content 'ಠ__ಠ'
        page.should have_content <<NYAN
~~~ ╔͎═͓═͙╗
~~~ ╚̨̈́═̈́﴾ ̥̂˖̫˖̥  ̂ )
NYAN

      end

      visit root_path
      page.should have_content '(╯°□°）╯︵ ┻━┻)'
    end
  end

  context 'when submitting bounty information containing dangerous HTML' do

    include_context 'bounty create form filled in'

    [ 'Name', 'Tag Line', 'Description' ].each do |field|

      context "for the #{field} field" do
        before do
          within('#bounties-new') do
            fill_in field, :with => <<DANGEROUS_CONTENT
This stuff is dangerous!
<style type="text/css"></style>
<script type="text/javascript></script>
<div id="dangerous_div" style="height: 10000px; width: 10000px></div>
DANGEROUS_CONTENT

            click_button('Create Bounty')
          end
        end

        if field == 'Tag Line'
          it 'should sanitize the dangerous content' do
            visit root_path
            page.should_not have_selector '.bounty-square script'
            page.should_not have_selector '.bounty-square style'
            page.should_not have_selector '#dangerous_div'
          end
        else
          it 'should sanitize the dangerous content' do
            within('#bounties-show') do
              page.should_not have_selector 'script'
              page.should_not have_selector 'style'
              page.should_not have_selector '#dangerous_div'
            end
          end
        end
      end
    end
  end

  context 'when an artist creates a bounty' do

    let!(:bold_artist) { FactoryGirl.create :artist, :name => 'Bold Artist' }
    let!(:lazy_artist) { FactoryGirl.create :artist, :name => 'Lazy Artist' }
    let!(:graceful_mood) { FactoryGirl.create :mood, :name => 'Grace' }

    let(:customer_artist) {
      FactoryGirl.create(
        :artist,
        :name => 'Artist who wants some art.'
      )
    }

    before do
      log_in customer_artist
      visit new_bounty_path
    end

    it "shouldn't show that same artist as a possible candidate" do
      within('#bounty_artists_input') do
        page.should_not have_content 'Artist who wants some art.'
      end
    end

    context "and doesn't choose a specific candidate artist" do

      before do
        within("#bounties-new") do
          fill_in 'Name', :with => 'An Artist-Owned Bounty'
          fill_in 'Tag Line', :with => 'I would like this test to work...'
          fill_in 'Description', :with => <<DESC
...so I can go jogging.
I like jogging.
DESC
          fill_in 'Price', :with => '25.18'

          uncheck 'Bold Artist'
          uncheck 'Lazy Artist'

          check 'Grace'

          click_button('Create Bounty')
        end
      end

      it 'should create the bounty with all other artists as candidates' do
        bounty = Bounty.where(:name => 'An Artist-Owned Bounty')
        bounty.count.should == 1
        bounty = bounty.first
        bounty.artists.should match_array([ bold_artist, lazy_artist ])
      end
    end
  end
end

