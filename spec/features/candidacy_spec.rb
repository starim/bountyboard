require 'spec_helper'

describe 'Candidacy' do

  context 'for an unclaimed bounty' do

    before {
      @candidate_artist = FactoryGirl.create(:artist)
      @unclaimed_bounty = FactoryGirl.create(:bounty,
        :artists => [ @candidate_artist ]
      )
    }

    it 'should allow accepting a bounty by a candidate artist' do

      log_in @candidate_artist
      visit bounty_path(@unclaimed_bounty)

      # hours were spent trying to turn on Capybara's "exact match" option, but
      # despite folloing the docs exactly, somehow Capybara absolutely refused
      # to do exact matches, so the following line could potentially match the
      # 'Un-Accept' button
      # page.should_not have_selector '.bounty-button', :text => 'Accept'
      page.should have_xpath '//*[@class="bounty-button"][text()="Accept"]'

      click_link 'Accept'
      @unclaimed_bounty.reload
      @unclaimed_bounty.acceptor_candidacy.should_not be_nil
      @unclaimed_bounty.acceptor_candidacy.artist.should == @candidate_artist
    end

    it 'should prevent accepting a bounty by a non-candidate artist' do

      other_artist = FactoryGirl.create(:artist)

      log_in other_artist
      visit bounty_path(@unclaimed_bounty)

      # hours were spent trying to turn on Capybara's "exact match" option, but
      # despite folloing the docs exactly, somehow Capybara absolutely refused
      # to do exact matches, so the following line would always fail because it
      # would match the 'Un-Accept' button
      # page.should_not have_selector '.bounty-button', :text => 'Accept'
      page.should_not have_xpath '//*[@class="bounty-button"][text()="Accept"]'
    end
  end

  context 'for a claimed bounty' do
    before {
      @acceptor_artist = FactoryGirl.create(:artist)
      @claimed_bounty = FactoryGirl.create(:bounty,
        :accepted,
        :artists => [ @acceptor_artist ]
      )
    }

    it 'should allow un-accepting a bounty by the accepting artist' do

      log_in @acceptor_artist
      visit '/auth/browser_id'
      visit bounty_path(@claimed_bounty)

      page.should have_selector '.bounty-button', :text => 'Un-Accept'
      click_link 'Un-Accept'
      @claimed_bounty.reload
      @claimed_bounty.acceptor_candidacy.should be_nil
    end

    it 'should prevent a candidate artist from accepting an already-accepted bounty' do

      @other_artist = FactoryGirl.create(:artist)
      @other_candidacy = FactoryGirl.create(:candidacy,
        :artist => @other_artist,
        :bounty => @claimed_bounty
      )
      @claimed_bounty.reload

      log_in @acceptor_artist
      visit bounty_path(@claimed_bounty)

      # hours were spent trying to turn on Capybara's "exact match" option, but
      # despite folloing the docs exactly, somehow Capybara absolutely refused
      # to do exact matches, so the following line would always fail because it
      # would match the 'Un-Accept' button
      # page.should_not have_selector '.bounty-button', :text => 'Accept'
      page.should_not have_xpath '//*[@class="bounty-button"][text()="Accept"]'
    end
  end

end
