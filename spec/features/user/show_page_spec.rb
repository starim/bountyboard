require 'spec_helper'

describe 'User#show page' do

  let!(:user) { FactoryGirl.create :user, :email => 'bestuser@example.com' }

  context "when a user visits who isn't the page owner" do
    before do
      log_in FactoryGirl.create :user
      visit user_path(user)
    end

    it 'should prevent the user from visiting the page' do
      current_path.should_not == user_path(user)
    end

    it "should inform the visitor why they weren't allowed to view the page" do
      page.should have_content(
        "You aren't allowed to visit a user's page that isn't yours."
      )
    end
  end

  context 'when an admin visits' do
    before do
      log_in FactoryGirl.create(:user, :admin)
      visit user_path(user)
    end

    it 'should allow the admin in' do
      current_path.should == user_path(user)
    end
  end

  context 'when the user has a known name' do
    before do
      user.name = 'Armistan Banes'
      user.save!

      log_in user
      visit user_path(user)
    end

    it "should show the user's name" do
      page.should have_selector '.page-area', :text => 'Armistan Banes'
    end
  end

  context "when the user doesn't have a known name" do

    before do
      log_in user
      visit user_path(user)
    end

    it "should show the user's email address" do
      page.should have_selector '.page-area', :text => 'bestuser@example.com'
    end
  end

  context 'when the user has a private bounty' do

    let!(:private_bounty) {
      FactoryGirl.create(
        :bounty,
        :private,
        :name => 'A Most Risqué Bounty',
        :owner => user
      )
    }

    before do
      log_in user
      visit user_path(user)
    end

    it 'should show the private bounty' do
      page.should have_content 'A Most Risqué Bounty'
    end
  end

  context 'when the user owns bounties of various statuses' do

    let!(:open_bounty) {
      FactoryGirl.create(
        :bounty,
        :name => 'Open Bounty',
        :owner => user
      )
    }
    let!(:accepted_bounty) {
      FactoryGirl.create(
        :bounty,
        :accepted,
        :name => 'Accepted Bounty',
        :owner => user
      )
    }
    let!(:completed_bounty) {
      FactoryGirl.create(
        :bounty,
        :completed,
        :owner => user
      )
    }

    before do
      log_in user
      visit user_path(user)
    end

    it 'should display the unclaimed bounty' do
      page.should have_content 'Open Bounty'
    end

    it 'should display the accepted bounty' do
      page.should have_content 'Accepted Bounty'
    end

    it 'should display the completed bounty' do
      page.should display_completed_bounty(completed_bounty.id)
    end
  end

  context 'when there are bounties in the system belonging to other users' do

    let!(:unrelated_bounty) {
      FactoryGirl.create :bounty, :name => 'Unrelated Bounty'
    }

    before do
      log_in user
      visit user_path(user)
    end

    it "shouldn't display bounties not owned by the User#show page's owner" do
      page.should_not have_content('Unrelated Bounty')
    end
  end
end
