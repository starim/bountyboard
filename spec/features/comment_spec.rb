require 'spec_helper'

describe 'Comment' do

  let(:test_bounty) { FactoryGirl.create(:bounty) }
  let(:test_user) { FactoryGirl.create(:user) }

  it 'should always be displayed on the associated bounty' do

    comment = FactoryGirl.create(
      :comment,
      :bounty => test_bounty,
      :content => 'This bounty sounds exciting.'
    )
    irrelevant_bounty = FactoryGirl.create(:bounty)
    irrelevant_comment = FactoryGirl.create(
      :comment,
      :bounty => irrelevant_bounty,
      :content => 'This bounty sucks.'
    )

    visit bounty_path(test_bounty)

    page.should have_selector '.comment', :count => 1
    page.should_not have_selector(
      '.comment-list *',
      :text => 'This bounty sucks.'
    )
    page.should have_selector(
      '.comment-list *',
      :text => 'This bounty sounds exciting'
    )
  end

  it "creation form should work" do

    log_in test_user
    visit bounty_path(test_bounty)

    fill_in 'comment[content]', :with => 'Comment testing user ID is set.'
    click_button('Publish Your Comment')

    new_comment = Comment.where(:bounty_id => test_bounty.id)
    new_comment.length.should == 1
    new_comment = new_comment.first
    new_comment.content.should == 'Comment testing user ID is set.'
    new_comment.user.should == test_user
  end

end

