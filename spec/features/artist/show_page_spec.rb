require 'spec_helper'

describe 'Artist#show page' do

  let(:artist) { FactoryGirl.create(:artist) }

  context 'when a generic user visits the page' do

    before do
      artist.name = 'Milt'
      artist.email = 'idrawthings@example.com'
      artist.created_at = Time.zone.parse('2013-05-18 13:32:45')
      artist.save!

      visit artist_path artist
    end

    it 'should redirect to root route when artist does not exist' do
      visit artist_path artist.id+1
      page.should have_content 'That artist does not exist.'
    end

    it "should show the artist's name" do
      page.should have_content('Milt')
    end

    it "should show the artist's email address" do
      page.should have_content('idrawthings@example.com')
    end

    it "should show the artist's join date" do
      page.should have_content('2013-05-18')
    end

    it 'should have a working Back button' do
      page.should have_selector(
        '.control-panel .bounty-button', :text => 'Back'
      )
    end

    it "shouldn't show update controls" do
      page.should_not have_selector(
        '.control-panel .bounty-button', :text => 'Update'
      )
    end
  end

  context 'when an admin visits the page' do
    let(:admin) { FactoryGirl.create :user, :admin }

    before do
      log_in admin
      visit artist_path artist
    end

    context 'for an un-approved artist' do

      before do
        artist.approved = false
        artist.save!
      end

      it 'should have a control for approving the artist' do
        page.should have_link 'Approve'
        # TODO: when we enable JavaScript in our tests, we can test
        # that this button actually works with the code below
        #
        # click_link 'Approve'
        # artist.approved.should be_true
      end
    end

    context 'for an approved artist' do

      before do
        artist.approved = true
        artist.save!
      end

      it 'should have a control for un-approving the artist' do
        page.should have_link 'Un-Approve'
        # TODO: when we enable JavaScript in our tests, we can test
        # that this button actually works with the code below
        #
        # click_link 'Un-Approve'
        # artist.approved.should be_false
      end
    end
  end

  [ 'bio', 'bounty_rules' ].each do |markdown_property|
    context "when the artist has Markdown in their #{markdown_property}" do

      let(:markdown_artist) {
        FactoryGirl.create(
          :artist,
          markdown_property.to_sym => <<MARKDOWN
1. asdf
2. *asdf*
3. **asdf**
MARKDOWN
        )
      }

      before { visit artist_path(markdown_artist) }

      it 'should display the Markdown converted to HTML' do
        within(".#{markdown_property.gsub('_', '-')}") do
          page.should have_selector 'ol li', :count => 3
          page.should have_selector 'li em'
          page.should have_selector 'li strong'
        end
      end
    end
  end

  context 'when the artist visits their own page' do

    before do
      log_in artist
      visit artist_path artist
    end

    it "should provide a link to the artist's dashboard" do
      click_link 'Your Dashboard'
      current_path.should == user_path(artist)
    end

    it "should provide a link to edit the artist's details" do
      click_link 'Update'
      current_path.should == edit_artist_path(artist)
    end
  end


  [ 'accepted', 'completed' ].each do |status|
    context "for an artist that has #{status} a bounty" do

      let(:customer) { FactoryGirl.create :user }
      let!(:public_bounty) {
        FactoryGirl.create(
          :bounty,
          status.to_sym,
          :owner => customer,
          :tag_line => 'Just Your Average Bounty, Everyone',
          :artists => [ artist ]
        )
      }
      let!(:adult_bounty) {
        FactoryGirl.create(
          :bounty,
          status.to_sym,
          :owner => customer,
          :tag_line => 'A Racy Bounty',
          :adult_only => true,
          :artists => [ artist ]
        )
      }
      let!(:private_bounty) {
        FactoryGirl.create(
          :bounty,
          status.to_sym,
          :owner => customer,
          :tag_line => 'A Private Bounty',
          :private => true,
          :artists => [ artist ]
        )
      }

      before do
        log_in viewer
        visit artist_path(artist)
      end

      # these user types should only see public bounties
      [ 'a guest', 'a generic user', 'another artist' ].each do |visitor_name|
        context "when #{visitor_name} visits" do
          case visitor_name
          when 'a guest'
            let(:viewer) { nil }
          when 'a generic user'
            let(:viewer) { FactoryGirl.create :user }
          when 'another artist'
            let(:viewer) { FactoryGirl.create :artist }
          end

          it 'should show the public bounty' do
            if status == 'completed'
              page.should display_completed_bounty(public_bounty.id)
            else
              page.should have_content 'Just Your Average Bounty, Everyone'
            end
          end

          it 'should hide the adult bounty' do
            if status == 'completed'
              page.should_not display_completed_bounty(adult_bounty.id)
            else
              page.should_not have_content 'A Racy Bounty'
            end
          end

          it 'should hide the private bounty' do
            if status == 'completed'
              page.should_not display_completed_bounty(private_bounty.id)
            else
              page.should_not have_content 'A Private Bounty'
            end
          end
        end
      end

      # these user types get to see bounties that would normally be hidden
      [ 'the owning artist', 'the customer', 'an admin' ].each do |visitor_name|
        context "when the #{visitor_name} visits" do
          case visitor_name
          when 'the owning artist'
            let(:viewer) { artist }
          when 'the customer'
            let(:viewer) { customer }
          when 'an admin'
            let(:viewer) { FactoryGirl.create :user, :admin }
          end

          it 'should show the public bounty' do
            if status == 'completed'
              page.should display_completed_bounty(public_bounty.id)
            else
              page.should have_content 'Just Your Average Bounty, Everyone'
            end
          end

          it 'should show the adult bounty' do
            if status == 'completed'
              page.should display_completed_bounty(adult_bounty.id)
            else
              page.should have_content 'A Racy Bounty'
            end
          end

          it 'should show the private bounty' do
            if status == 'completed'
              page.should display_completed_bounty(private_bounty.id)
            else
              page.should have_content 'A Private Bounty'
            end
          end
        end
      end
    end
  end

  [ 'accepted', 'completed' ].each do |status|
    context "when the artist has #{status} many bounties " do

      before do
        FactoryGirl.create_list(
          :bounty,
          2 * ArtistsController.BOUNTIES_PER_SECTION,
          status.to_sym,
          :artists => [ artist ]
       )

        visit artist_path(artist)
      end

      it 'should display only a limited selection of bounties' do
        page.should(
          display_this_many_bounties ArtistsController.BOUNTIES_PER_SECTION
        )
      end
    end
  end
end
