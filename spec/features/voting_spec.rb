require 'spec_helper'
include VotesHelper::VoteType

describe 'Voting controls' do

  let!(:sample_bounty) { FactoryGirl.create(:bounty) }

  context 'when a guest visits' do
    before { visit root_path }

    it 'should not display voting controls' do
      page.should_not have_selector('.bounty-square .upvote')
      page.should_not have_selector('.bounty-square .downvote')
    end
  end

  context 'when a logged-in user visits' do
    before do
      log_in FactoryGirl.create(:user)
      visit root_path
    end

    it 'should display voting controls on each bounty' do
      within('.bounty-square') do
        page.should have_selector('.upvote')
        page.should have_selector('.downvote')
      end
    end
  end

  context 'when a logged-in bounty owner visits' do

    let(:customer) { FactoryGirl.create(:user) }
    let!(:customer_bounty) { FactoryGirl.create(:bounty, :owner => customer) }

    before do
      log_in customer
      visit root_path
    end

    it 'should only display voting controls on bounties not owned by that user' do
      within(".bounty-square[data-id='#{sample_bounty.id}']") do
        page.should have_selector('.upvote')
        page.should have_selector('.downvote')
      end
    end
  end

  context 'when a user votes' do

    shared_examples_for 'check vote was cast' do |expected_vote_type|
      it 'should register a vote' do
        Vote.count.should == 1
      end
      subject { Vote.first }
      its(:user) { should == voter }
      its(:bounty) { should == sample_bounty }
      its(:vote_type) { should == expected_vote_type }
    end

    let(:voter) { FactoryGirl.create :user }

    context 'by clicking the upvote link' do
      before do
        log_in voter
        visit root_path
        find('.bounty-square .upvote').click
      end

      include_examples 'check vote was cast', VoteType::UPVOTE
    end

    context 'when the downvote link is clicked' do
      before do
        log_in voter
        visit root_path
        find('.bounty-square .downvote').click
      end

      include_examples 'check vote was cast', VoteType::DOWNVOTE
    end
  end
end

