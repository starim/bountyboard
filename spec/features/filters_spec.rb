require 'spec_helper'

describe 'Bounty list' do

  shared_context 'basic bounties for testing default filters' do

    let(:generic_user) { FactoryGirl.create(:user) }
    let(:accepting_artist) { FactoryGirl.create(:artist) }
    let(:customer) { FactoryGirl.create(:user) }
    let(:admin) { FactoryGirl.create(:user, :admin) }

    before do
      # create a bounty with nothing special about it
      FactoryGirl.create(
        :bounty,
        :accepted,
        :name => 'Normal Bounty',
        :adult_only => false,
        :private => false,
        :price => 100.00,
        :owner => customer,
        :artists => [ accepting_artist ]
      )
      # create an adult-only bounty
      FactoryGirl.create(
        :bounty,
        :accepted,
        :name => 'Adult Bounty',
        :adult_only => true,
        :private => false,
        :price => 100.00,
        :owner => customer,
        :artists => [ accepting_artist ]
      )
      # create a private bounty
      FactoryGirl.create(
        :bounty,
        :accepted,
        :name => 'Private Bounty',
        :adult_only => false,
        :private => true,
        :price => 100.00,
        :owner => customer,
        :artists => [ accepting_artist ]
      )
    end
  end

  # Tests that the default filters work, assuming the 'basic bounties for
  # testing default filters' shared context has been loaded.
  #
  # Parameters:
  #   admin:
  #     The output of the private bounty filter is different for admins, so
  #     if an admin is logged in pass true to this parameter.
  #   skip_tests:
  #     In case the conditions being tested modify some of the default
  #     filters from their normal values, it is possible to skip checking one
  #     of the default filters by passing its name to the skip_tests
  #     parameter: 'normal' to skip testing whether normal bounties show up
  #     on the page, 'adult' to skip testing that adult bounties are hidden,
  #     and 'private' to skip checking that private bounties are hidden (or
  #     shown if the user is admin)
  shared_examples_for 'default filters' do
    |admin: false, skip_tests: []|

    skip_tests = Array(skip_tests)

    if !skip_tests.include? 'normal'
      it 'should display normal bounties' do
        page.should have_selector '.bounty-square', :text => 'Normal Bounty'
      end
    end

    if !skip_tests.include? 'adult'
      it 'should not display bounties with adult content' do
        page.should_not have_selector(
          '.bounty-square',
          :text => 'Adult Bounty'
        )
      end
    end

    if !skip_tests.include? 'private'
      if admin
        it 'should display private bounties' do
          page.should have_selector(
            '.bounty-square',
            :text => 'Private Bounty'
          )
        end
      else
        it 'should not display private bounties' do
          page.should_not have_selector(
            '.bounty-square',
            :text => 'Private Bounty'
          )
        end
      end
    end
  end

  context 'with default filters' do

    include_context 'basic bounties for testing default filters'

    before do
      log_in user
      visit root_path
    end

    context 'when a guest visits' do
      let(:user) { nil }

      include_examples 'default filters'
    end

    context 'when a logged-in user visits' do
      let(:user) { generic_user }

      include_examples 'default filters'
    end

    context 'when a logged-in bounty owner visits' do
      let(:user) { customer }

      include_examples 'default filters', skip_tests: 'private'

      it 'should display private bounties owned by that user' do
        page.should have_selector '.bounty-square', :text => 'Private Bounty'
      end
    end

    context 'when the accepting artist visits' do
      let(:user) { accepting_artist }

      include_examples 'default filters', skip_tests: 'private'

      it 'should display private bounties accepted by that artist' do
        page.should have_selector '.bounty-square', :text => 'Private Bounty'
      end
    end

    context 'when a logged-in admin visits' do
      let(:user) { admin }

      include_examples 'default filters', admin: true
    end
  end

  context 'with a price filter' do

    include_context 'basic bounties for testing default filters'

    before do
      FactoryGirl.create(
        :bounty,
        :name => 'Cheap Bounty',
        :price => 15.00
      )
      FactoryGirl.create(
        :bounty,
        :name => 'Moderately-Priced Bounty',
        :price => 100.00
      )
      FactoryGirl.create(
        :bounty,
        :name => 'Expensive Bounty',
        :price => 1500.00
      )

      visit root_path + '?price_min=15.00&price_max=500.00'
    end

    include_examples 'default filters'

    it 'should not display a bounty less than the price filter range' do
      page.should_not have_selector '.bounty-square', :text => 'Cheap Bounty'
    end

    it 'should display bounties in the price filter range' do
      page.should have_selector(
        '.bounty-square',
        :text => 'Moderately-Priced Bounty'
      )
    end

    it 'should not display a bounty greater than the price filter range' do
      page.should_not have_selector(
        '.bounty-square',
        :text => 'Expensive Bounty'
      )
    end
  end

  context 'with the adult content filter' do

    include_context 'basic bounties for testing default filters'

    context 'turned off' do

      before { visit root_path + '?adult=all' }

      include_examples 'default filters', skip_tests: 'adult'

      it 'should display bounties with adult content' do
        page.should have_selector '.bounty-square', :text => 'Adult Bounty'
      end
    end

    context 'set to only show adult bounties' do

      before { visit root_path + '?adult=adult' }

      include_examples 'default filters', skip_tests: ['normal', 'adult']

      it "shouldn't display bounties with no adult content" do
        page.should_not have_selector(
          '.bounty-square',
          :text => 'Normal Bounty'
        )
      end

      it 'should display bounties with adult content' do
        page.should have_selector '.bounty-square', :text => 'Adult Bounty'
      end
    end
  end

  context 'with an ownership filter' do

    let(:owner) { FactoryGirl.create(:user) }

    include_context 'basic bounties for testing default filters'

    before {
      FactoryGirl.create(:bounty, :name => 'Owned Bounty', :owner => owner)

      log_in owner
      visit root_path + '?own=1'
    }

    it 'should display owned bounties' do
      page.should have_selector '.bounty-square', :text => 'Owned Bounty'
    end

    it 'should not display bounties not owned by the logged in user' do
      page.should display_this_many_bounties 1
    end
  end

  context "with an artist's candidacy filter" do

    let(:other_artist) { FactoryGirl.create(:artist) }
    let(:artist) { FactoryGirl.create(:artist, :name => 'Fancy Artist') }

    include_context 'basic bounties for testing default filters'

    before do
      FactoryGirl.create(
        :bounty,
        :name => 'Candidate Bounty',
        :artists => [ other_artist, artist ]
      )

      visit root_path + '?candidacy=Fancy%20Artist'
    end

    it 'should display bounties which the artist is a candidate for' do
      page.should have_selector '.bounty-square', :text => 'Candidate Bounty'
    end

    it 'should not display bounties where the artist is not a candidate' do
      page.should display_this_many_bounties 1
    end
  end

  context 'with the status filter' do
    before do
      FactoryGirl.create(
        :bounty,
        :name => 'Unclaimed Bounty'
      )
      FactoryGirl.create(
        :bounty,
        :name => 'Adult Unclaimed Bounty',
        :adult_only => true
      )
      FactoryGirl.create(
        :bounty,
        :name => 'Private Unclaimed Bounty',
        :private => true
      )

      FactoryGirl.create(
        :bounty,
        :accepted,
        :name => 'Accepted Bounty'
      )
      FactoryGirl.create(
        :bounty,
        :accepted,
        :name => 'Adult Accepted Bounty',
        :adult_only => true
      )
      FactoryGirl.create(
        :bounty,
        :accepted,
        :name => 'Private Accepted Bounty',
        :private => true
      )

      FactoryGirl.create(
        :bounty,
        :completed,
        :name => 'Completed Bounty'
      )
      FactoryGirl.create(
        :bounty,
        :completed,
        :name => 'Adult Completed Bounty',
        :adult_only => true
      )
      FactoryGirl.create(
        :bounty,
        :completed,
        :name => 'Private Completed Bounty',
        :private => true
      )
    end

    context 'set to "Unclaimed"' do
      before { visit root_path + '?status=Unclaimed' }

      it 'should only display unclaimed bounties' do
        page.should have_selector(
          '.bounty-square',
          :text => 'Unclaimed Bounty'
        )
      end

      it 'should still apply default filters' do
        page.should display_this_many_bounties 1
      end
    end

    context 'set to "Accepted"' do
      before { visit root_path + '?status=Accepted' }

      it 'should only display accepted bounties' do
        page.should have_selector(
          '.bounty-square',
          :text => 'Accepted Bounty'
        )
      end

      it 'should still apply default filters' do
        page.should display_this_many_bounties 1
      end
    end

    context 'set to "Completed"' do
      before { visit root_path + '?status=Completed' }

      it 'should only display completed bounties' do
        page.should have_selector(
          '.bounty-square[data-filename="test.png"]'
        )
      end

      it 'should still apply default filters' do
        page.should display_this_many_bounties 1
      end
    end
  end

  context 'with artist workload filter' do

    let(:artist) { FactoryGirl.create(:artist, :name => 'Busy Artist') }
    let(:other_artist) { FactoryGirl.create(:artist) }

    include_context 'basic bounties for testing default filters'

    before do
      FactoryGirl.create(
        :bounty,
        :name => 'Open Bounty',
        :artists => [ artist, other_artist ]
      )
      FactoryGirl.create(
        :bounty,
        :name => 'Claimed Bounty By Wrong Artist',
        :artists => [ other_artist ]
      )
      FactoryGirl.create(
        :bounty,
        :accepted,
        :name => 'Claimed Bounty',
        :artists => [ artist ]
      )
      FactoryGirl.create(
        :bounty,
        :completed,
        :name => 'Completed Bounty',
        :artists => [ artist ]
      )

      visit root_path + '?artist_workload=Busy%20Artist'
    end

    it 'should only display bounties accepted by the specified artist' do
      page.should have_selector '.bounty-square', :text => 'Claimed Bounty'
      page.should display_this_many_bounties 1
    end
  end

  context 'with artist portfolio filter' do

    let(:artist) { FactoryGirl.create(:artist, :name => 'Busy Artist') }
    let(:other_artist) { FactoryGirl.create(:artist) }

    include_context 'basic bounties for testing default filters'

    before do
      FactoryGirl.create(
        :bounty,
        :name => 'Open Bounty',
        :artists => [ artist, other_artist ]
      )
      FactoryGirl.create(
        :bounty,
        :accepted,
        :name => 'Accepted Bounty',
        :artists => [ artist ]
      )
      FactoryGirl.create(
        :bounty,
        :completed,
        :name => 'Completed Bounty',
        :artists => [ artist ]
      )

      visit root_path + '?artist_portfolio=Busy%20Artist'
    end

    it 'should only display bounties completed by the specified artist' do
      page.should display_this_many_bounties 1
      page.should have_selector(
        '.bounty-square[data-name="Completed Bounty"]'
      )
    end
  end
end

