require 'spec_helper'

describe 'Bounty favorite controls' do

  let!(:bounty) { FactoryGirl.create :bounty }
  let(:visitor) { FactoryGirl.create :user }

  it 'should not appear if the visitor is not logged in' do
    visit root_path
    page.should_not have_selector '.fav'
    page.should_not have_selector '.fav-red'
  end

  context 'on a bounty that is not already a favorite' do

    before do
      log_in visitor
      visit root_path
    end

    it 'should have their normal appearance' do
      page.should have_selector '.fav'
    end

    it 'should allow the user to favorite the bounty' do
      first('.fav').click
      visitor.favorite_bounties.should == [ bounty ]
    end
  end

  context 'on a bounty that is already a favorite' do

    before do
      FactoryGirl.create :favorite, :user => visitor, :bounty => bounty
      log_in visitor
      visit root_path
    end

    it 'should have a modified appearance' do
      page.should have_selector '.fav.fav-red'
    end

    it 'should allow the user to unfavorite the bounty' do
        first('.fav.fav-red').click
        visitor.favorite_bounties.count.should == 0
    end
  end
end
