# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'rspec/autorun'
require 'email_spec'
require 'paperclip/matchers'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

RSpec.configure do |config|
  # ## Mock Framework
  #
  # If you prefer to use mocha, flexmock or RR, uncomment the appropriate line:
  #
  # config.mock_with :mocha
  # config.mock_with :flexmock
  # config.mock_with :rr

  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  # If true, the base class of anonymous controllers will be inferred
  # automatically. This will be the default behavior in future versions of
  # rspec-rails.
  config.infer_base_class_for_anonymous_controllers = false

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = "random"

  # set spec type by the file location (integration tests in
  # /spec/features/, model specs in /spec/models/, etc)
  config.infer_spec_type_from_file_location!

  # make the test log readable by printing a heading naming the test the
  # following messages belong to.
  config.before(:each) do
    full_example_description = "Starting #{self.class.description} #{@method_name}"
    Rails::logger.info("\n\n#{full_example_description}\n#{'-' * (full_example_description.length)}")
  end

  config.include(EmailSpec::Helpers)
  config.include(EmailSpec::Matchers)

  config.include Paperclip::Shoulda::Matchers

  # This helper function takes a user model and logs in that user. If the
  # passed user object is nil, no action is taken. For it to work, OmniAuth
  # must be set to test mode. Make sure this line appears in
  # config/environments/test.rb:
  # OmniAuth.config.test_mode = true
  def log_in(user)
    if user
      OmniAuth.config.mock_auth[:browser_id] = OmniAuth::AuthHash.new({
        :provider => 'browserid',
        :uid => user.email
      })

      # for integration tests, actually log in by visiting using the login API;
      # for controller and request specs, just stub out
      # ApplicationController.current_user() so that it always reports the
      # expected user as logged in
      if (defined? visit)
        visit '/auth/browser_id'
      else
        ApplicationController.any_instance.stub(:current_user).and_return(user)
      end
    end
  end

end

