# == Schema Information
#
# Table name: comments
#
#  id         :integer          not null, primary key
#  bounty_id  :integer          not null
#  user_id    :integer          not null
#  content    :text             not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'spec_helper'

describe Comment do

  it { should respond_to(:bounty) }
  it { should respond_to(:user) }
  it { should respond_to(:content) }

  it 'should require its attached bounty be set' do
    invalid_message = FactoryGirl.build(:comment, :bounty => nil)
    invalid_message.should_not be_valid
    invalid_message.should have(1).error_on(:bounty)
  end

  it 'should require its owning user be set' do
    invalid_message = FactoryGirl.build(:comment, :user => nil)
    invalid_message.should_not be_valid
    invalid_message.should have(1).error_on(:user)
  end

  it 'should not allow its content to be blank' do
    invalid_message = FactoryGirl.build(:comment, :content => nil)
    invalid_message.should_not be_valid
    invalid_message.should have(1).error_on(:content)

    invalid_message = FactoryGirl.build(:comment, :content => '')
    invalid_message.should_not be_valid
    invalid_message.should have(1).error_on(:content)
  end
end
