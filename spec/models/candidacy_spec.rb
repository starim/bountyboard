# == Schema Information
#
# Table name: candidacies
#
#  id          :integer          not null, primary key
#  bounty_id   :integer          not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  artist_id   :integer          not null
#  accepted_at :datetime
#

require 'spec_helper'

describe Candidacy do

  it { should respond_to(:id) }
  it { should respond_to(:bounty) }
  it { should respond_to(:accepted_at) }

  before {
    @bounty = FactoryGirl.create(:bounty)
    @artist = FactoryGirl.create(:artist)
  }

  it 'should not allow more than one acceptor per bounty' do
    candidacy1 = FactoryGirl.build(:candidacy,
      :accepted_at => 1.seconds.from_now,
      :bounty => @bounty,
      :artist => @artist
    )
    candidacy1.should be_valid
    candidacy1.save!

    artist2 = FactoryGirl.create(:artist)
    candidacy2 = FactoryGirl.build(:candidacy,
      :accepted_at => 2.seconds.from_now,
      :bounty => @bounty,
      :artist => artist2
    )
    candidacy2.should_not be_valid
    candidacy2.should have(1).error_on(:accepted_at)
  end

  it 'should not allow null values for its bounty property' do
    candidacy = FactoryGirl.build(:candidacy,
      :bounty => nil,
      :artist => @artist
    )
    candidacy.should_not be_valid
    candidacy.should have(1).error_on(:bounty)
  end

  it 'should not allow null values for its artist property' do
    candidacy = FactoryGirl.build(:candidacy,
      :bounty => @bounty,
      :artist => nil
    )
    candidacy.should_not be_valid
    candidacy.should have(1).error_on(:artist)
  end

  # because of the complexity of the validator that each bounty has at most one
  # accepting candidacy, there were problems with the validator preventing an
  # acceptor candidacies' properties from being changed and saved
  context 'that is an acceptor of a bounty' do
    let(:bounty) { FactoryGirl.create(:bounty, :accepted) }

    it 'should allow updating its attributes' do
      candidacy = bounty.acceptor_candidacy
      candidacy.should_not be_nil
      candidacy.accepted_at = DateTime.strptime(
          '2014-01-07 Pacific Time (US & Canada)',
          '%Y-%m-%d %Z'
      )
      candidacy.should be_valid
    end
  end

  context 'for a bounty owned by an artist' do

    let(:artist_with_bounty) { FactoryGirl.create :artist }
    let(:other_artist) { FactoryGirl.create :artist }
    let(:artist_owned_bounty) {
      FactoryGirl.create(
        :bounty,
        :owner => artist_with_bounty,
        :artists => [ other_artist ]
      )
    }

    it 'should not allow the owning artist to be a candidate' do
      invalid_candidacy = Candidacy.new(
        :bounty => artist_owned_bounty,
        :artist => artist_with_bounty
      )

      invalid_candidacy.should_not be_valid
      invalid_candidacy.should have(1).error_on :artist
    end
  end

  describe 'database trigger preventing multiple acceptors on the same bounty' do
    context 'for a candidacy that already has an acceptor' do

      let!(:acceptor) { FactoryGirl.create :candidacy, :acceptor }
      let!(:nonacceptor) {
        FactoryGirl.create :candidacy, :bounty => acceptor.bounty
      }

      it 'should prevent creating an extra acceptor candidacy' do
        extra_acceptor = FactoryGirl.build(
          :candidacy,
          :acceptor,
          :bounty => acceptor.bounty
        )

        expect {
          extra_acceptor.save!(:validate => false)
        }.to raise_error(ActiveRecord::StatementInvalid)
      end

      it 'should prevent updating a non-acceptor candidacy to be an acceptor' do
        nonacceptor.accepted_at = Time.at(1400000000)
        expect {
          nonacceptor.save!(:validate => false)
        }.to raise_error(ActiveRecord::StatementInvalid)
      end

      it 'should allow creating an extra non-acceptor candidacy' do
        FactoryGirl.create :candidacy, :bounty => acceptor.bounty
      end

      it 'should allow updating the acceptor candidacy to be a non-acceptor' do
        acceptor.accepted_at = nil
        acceptor.save!
      end
    end

    context 'for a candidacy with no acceptor' do

      let!(:existing_candidacy) { FactoryGirl.create :candidacy }

      it 'should allow creating a new acceptor candidacy' do
        FactoryGirl.create(
          :candidacy,
          :acceptor,
          :bounty => existing_candidacy.bounty
        )
      end

      it 'should allow updating a non-acceptor candidacy to be an acceptor' do
        existing_candidacy.accepted_at = Time.at(1400000000)
        existing_candidacy.save!
      end

      it 'should allow creating an extra non-acceptor candidacy' do
        FactoryGirl.create :candidacy, :bounty => existing_candidacy.bounty
      end
    end
  end
end
