# == Schema Information
#
# Table name: messages
#
#  id                            :integer          not null, primary key
#  conversation_participation_id :integer          not null
#  content                       :text             not null
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#

require 'spec_helper'

describe Message do

  it { should respond_to(:conversation) }
  it { should respond_to(:user) }
  it { should respond_to(:content) }

  it 'should require its conversation participant be set' do
    invalid_message = FactoryGirl.build(
      :message,
      :conversation_participation => nil
    )
    invalid_message.should_not be_valid
    invalid_message.should have(1).error_on(:conversation_participation)
  end

  it 'should not allow its content to be blank' do
    invalid_message = FactoryGirl.build(:message, :content => nil)
    invalid_message.should_not be_valid
    invalid_message.should have(1).error_on(:content)

    invalid_message = FactoryGirl.build(:message, :content => '')
    invalid_message.should_not be_valid
    invalid_message.should have(1).error_on(:content)
  end
end
