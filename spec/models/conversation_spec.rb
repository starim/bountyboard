# == Schema Information
#
# Table name: conversations
#
#  id         :integer          not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'spec_helper'

describe Conversation do

  it { should respond_to(:conversation_participations) }
  it { should respond_to(:users) }
  it { should respond_to(:messages) }

  it 'should not allow less than 2 participants' do
    invalid_conversation = FactoryGirl.build(
      :conversation,
      :conversation_participation_count => 1
    )
    invalid_conversation.should_not be_valid
    invalid_conversation.should have(1).error_on(:conversation_participations)
  end

  it 'should order its associated messages by create date ascending' do
    conversation = FactoryGirl.build(:conversation)
    conversation.conversation_participations[0].messages = [
      FactoryGirl.build(:message,
                        :created_at => DateTime.strptime('1382978371','%s')
                       ),
      FactoryGirl.build(:message,
                        :created_at => DateTime.strptime('1173081600','%s')
                       )
    ]
    conversation.conversation_participations[1].messages = [
      FactoryGirl.build(:message,
                        :created_at => DateTime.strptime('1325404800','%s')
                       )
    ]

    conversation.save!
    conversation.reload

    conversation.messages[0].created_at.should == DateTime.strptime('1173081600','%s')
    conversation.messages[1].created_at.should == DateTime.strptime('1325404800','%s')
    conversation.messages[2].created_at.should == DateTime.strptime('1382978371','%s')
  end

end

