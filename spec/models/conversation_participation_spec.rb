# == Schema Information
#
# Table name: conversation_participations
#
#  id              :integer          not null, primary key
#  conversation_id :integer          not null
#  user_id         :integer          not null
#

require 'spec_helper'

describe ConversationParticipation do

  it { should respond_to(:conversation) }
  it { should respond_to(:user) }
  it { should respond_to(:messages) }

  it 'should require its conversation be set' do
    invalid_conversation_participation =
      FactoryGirl.build(:conversation_participation, :conversation => nil)
    invalid_conversation_participation.should_not be_valid
    invalid_conversation_participation.should
      have(1).error_on(:conversation_participations)
  end

  it 'should require its user be set' do
    invalid_conversation_participation =
      FactoryGirl.build(:conversation_participation, :user => nil)
    invalid_conversation_participation.should_not be_valid
    invalid_conversation_participation.should have(1).error_on(:user)
  end

  it 'should not allow redundant participation declarations' do
    user = FactoryGirl.create(:user)
    valid_participation = FactoryGirl.build(
      :conversation_participation,
      :user => user
    )
    valid_participation.should be_valid
    valid_participation.save!

    redundant_participation = FactoryGirl.build(:conversation_participation,
      :conversation => valid_participation.conversation,
      :user => user
    )
    redundant_participation.should_not be_valid
    redundant_participation.should have(1).error_on(:user_id)
  end
end

