require 'spec_helper'

# These tests ensure that actions reached by GET requests enforce the expected
# user access restrictions. All other actions have side-effects and can't be
# tested in a generic way. Testing whether non-GET request actions follow the
# expected user access restrictions should be part of the controller-specific
# integration tests for those actions.
describe 'Access restrictions on the' do

  shared_examples_for 'allows universal access' do

    context "when a guest visits" do
      before do
        get(
          url,
          nil,
          { 'HTTP_REFERER' => previous_url }
        )
      end

      it 'should allow access to the page' do
        response.should be_success
      end
    end
  end

  shared_examples_for 'only allows logged in users access' do

    context "when a guest visits" do

      before do
        get(
          url,
          nil,
          { 'HTTP_REFERER' => previous_url }
        )
      end

      it 'should redirect the guest away from the page' do
        response.should redirect_to(previous_url)
      end
    end

    context 'when a logged in user visits' do

      before do
        log_in FactoryGirl.create(:user)
        get(
          url,
          nil,
          { 'HTTP_REFERER' => previous_url }
        )
      end

      it 'should allow access to the page' do
        response.should be_success
      end
    end
  end

  shared_examples_for 'only allows admin access' do

    context "when a guest visits" do

      before do
        get(
          url,
          nil,
          { 'HTTP_REFERER' => previous_url }
        )
      end

      it 'should redirect the guest away from the page' do
        response.should redirect_to(previous_url)
      end
    end

    context 'when a generic logged-in user visits' do

      before do
        log_in FactoryGirl.create(:user)
        get(
          url,
          nil,
          { 'HTTP_REFERER' => previous_url }
        )
      end

      it 'should redirect the user away from the page' do
        response.should redirect_to(previous_url)
      end
    end

    context 'when an admin visits' do

      before do
        log_in FactoryGirl.create(:user, :admin)
        get(
          url,
          nil,
          { 'HTTP_REFERER' => previous_url }
        )
      end

      it 'should allow the admin to reach the page' do
        response.should be_success
      end
    end
  end




  describe 'Bounty#index action' do
    include_examples 'allows universal access' do
      let(:url) { bounties_url }
      let(:previous_url) { root_url }
    end
  end

  describe 'Bounty#new action' do
    include_examples 'only allows logged in users access' do
      let(:url) { new_bounty_url }
      let(:previous_url) { bounties_url }
    end
  end

  describe 'Bounty#show action' do
    include_examples 'allows universal access' do
      let(:bounty) { FactoryGirl.create(:bounty) }
      let(:url) { bounty_url(bounty) }
      let(:previous_url) { new_bounty_url }
    end
  end

  describe 'Bounty#edit action' do
    include_examples 'only allows admin access' do
      let(:bounty) { FactoryGirl.create(:bounty) }
      let(:url) { edit_bounty_url(bounty) }
      let(:previous_url) { bounty_url(bounty) }
    end

    # the bounty owner should also be allowed access
    context 'when the bounty owner visits' do

      let(:owner) { FactoryGirl.create :user }
      let!(:bounty) { FactoryGirl.create :bounty, :owner => owner }

      before do
        log_in owner
        get(
          edit_bounty_url(bounty),
          nil,
          { 'HTTP_REFERER' => bounty_url(bounty) }
        )
      end

      it 'should allow the owner to reach the page' do
        response.should be_success
      end
    end
  end

  describe 'Bounty#complete action' do
    # Bounty#complete is unique in that it can only be accessed by the
    # accepting artist, hence it uses custom test logic instead of a shared
    # example

    let(:customer) { FactoryGirl.create :user }
    let(:accepting_artist) { FactoryGirl.create :artist }
    let!(:bounty) {
      FactoryGirl.create(
        :bounty,
        :accepted,
        :owner => customer,
        :artists => [ accepting_artist ]
      )
    }

    let(:url) { complete_bounty_url(bounty) }
    let(:previous_url) { bounty_url(bounty) }

    context "when a guest visits" do

      before do
        get(
          url,
          nil,
          { 'HTTP_REFERER' => previous_url }
        )
      end

      it 'should redirect the guest away from the page' do
        response.should redirect_to(previous_url)
      end
    end

    context "when a user who doesn't own the bounty visits" do

      before do
        log_in FactoryGirl.create(:user)
        get(
          url,
          nil,
          { 'HTTP_REFERER' => previous_url }
        )
      end

      it 'should redirect the user away from the page' do
        response.should redirect_to(previous_url)
      end
    end

    context 'when an admin visits' do

      before do
        log_in FactoryGirl.create(:user, :admin)
        get(
          url,
          nil,
          { 'HTTP_REFERER' => previous_url }
        )
      end

      it 'should redirect the user away from the page' do
        response.should redirect_to(previous_url)
      end
    end

    context 'when the bounty owner visits' do

      before do
        log_in customer
        get(
          url,
          nil,
          { 'HTTP_REFERER' => previous_url }
        )
      end

      it 'should redirect the owner away from the page' do
        response.should redirect_to(previous_url)
      end
    end

    context 'when the accepting artist visits' do

      before do
        log_in accepting_artist
        get(
          url,
          nil,
          { 'HTTP_REFERER' => previous_url }
        )
      end

      it 'should allow the artist to reach the page' do
        response.should be_success
      end
    end
  end

  describe 'Artist#index action' do
    include_examples 'allows universal access' do
      let(:url) { artists_url }
      let(:previous_url) { root_url }
    end
  end

  describe 'Artist#new action' do
    include_examples 'only allows admin access' do
      let(:url) { new_artist_url }
      let(:previous_url) { artists_url }
    end
  end

  describe 'Artist#show action' do
    include_examples 'allows universal access' do
      let(:artist) { FactoryGirl.create(:artist) }
      let(:url) { artist_url(artist) }
      let(:previous_url) { artists_url }
    end
  end

  describe 'Artist#edit action' do
    include_examples 'only allows admin access' do
      let(:artist) { FactoryGirl.create(:artist) }
      let(:url) { edit_artist_url(artist) }
      let(:previous_url) { artist_url(artist) }
    end

    # the artist should also be allowed access to their own edit page
    context 'when the owning artist visits' do

      let!(:artist) { FactoryGirl.create :artist }

      before do
        log_in artist
        get(
          edit_artist_url(artist),
          nil,
          { 'HTTP_REFERER' => artist_url(artist) }
        )
      end

      it 'should allow the artist to reach the page' do
        response.should be_success
      end
    end
  end

  describe 'User#show action' do
    include_examples 'only allows admin access' do
      let(:user) { FactoryGirl.create(:user) }
      let(:url) { user_url(user) }
      let(:previous_url) { root_url }
    end

    # the owning user should also be allowed access to their own show page
    context 'when the owning user visits' do

      let!(:user) { FactoryGirl.create :user }

      before do
        log_in user
        get(
          user_url(user),
          nil,
          { 'HTTP_REFERER' => root_url }
        )
      end

      it 'should allow the user to reach the page' do
        response.should be_success
      end
    end
  end
end
