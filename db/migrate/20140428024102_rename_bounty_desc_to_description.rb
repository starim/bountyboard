class RenameBountyDescToDescription < ActiveRecord::Migration
  def change
    rename_column :bounties, :desc, :description
  end
end
