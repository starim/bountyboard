class ChangeBountyNameToCitext < ActiveRecord::Migration
  def up
    execute "CREATE EXTENSION IF NOT EXISTS citext WITH SCHEMA public"
    execute "ALTER TABLE bounties ALTER name TYPE citext"
  end

  def down
    execute "ALTER TABLE bounties ALTER name TYPE character varying(255) "
  end
end
