class AddDatabaseCheckConstraintForMultipleAccetors < ActiveRecord::Migration
  def up
    execute <<-SQL
CREATE OR REPLACE FUNCTION validate_single_acceptor() RETURNS trigger AS $$
DECLARE
    adding_new_acceptor          boolean;
    id_of_existing_acceptor      integer;
BEGIN
    -- check if this operation is trying add a new acceptor
    IF (TG_OP = 'UPDATE') THEN
      adding_new_acceptor =
          OLD.accepted_at IS NULL AND NEW.accepted_at IS NOT NULL;
    ELSE
      adding_new_acceptor = NEW.accepted_at IS NOT NULL;
    END IF;


    -- if the operation isn't trying to add a new acceptor then let it proceed
    IF (NOT adding_new_acceptor) THEN
        RETURN NEW;
    END IF;

    -- if the operation is adding a new acceptor then fail if one already
    -- exists, otherwise let it proceed
    SELECT INTO id_of_existing_acceptor id FROM candidacies
    WHERE bounty_id=NEW.bounty_id AND accepted_at IS NOT NULL;

    IF (id_of_existing_acceptor IS NOT NULL) THEN
        RAISE EXCEPTION 'Cannot assign a new acceptor because candidacy % has already accepted the same bounty.',
            id_of_existing_acceptor;
    ELSE
        RETURN NEW;
    END IF;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER validate_single_acceptor_trigger
    BEFORE INSERT OR UPDATE OF accepted_at ON candidacies
    FOR EACH ROW EXECUTE PROCEDURE validate_single_acceptor();
SQL
  end

  def down
    execute 'DROP FUNCTION IF EXISTS validate_single_acceptor() CASCADE;'
  end
end
