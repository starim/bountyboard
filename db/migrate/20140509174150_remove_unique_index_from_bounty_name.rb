class RemoveUniqueIndexFromBountyName < ActiveRecord::Migration
  def up
    remove_index :bounties, :name
  end

  def down
    add_index :bounties, :name, :unique => true
  end
end
