class RenameRememberToken < ActiveRecord::Migration
  def change
    rename_column :users, :rememberToken, :remember_token
  end
end
