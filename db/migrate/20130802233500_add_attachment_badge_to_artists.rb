class AddAttachmentBadgeToArtists < ActiveRecord::Migration
  def self.up
    change_table :users do |t|
      t.attachment :badge
    end
  end

  def self.down
    drop_attached_file :users, :badge
  end
end