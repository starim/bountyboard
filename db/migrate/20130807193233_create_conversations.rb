class CreateConversations < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.references :bounty,       :null => false
      t.references :user,         :null => false
      t.text       :content,      :null => false
      t.timestamps
    end

    add_foreign_key :comments, :bounties
    add_foreign_key :comments, :users

    create_table :conversations do |t|
      t.timestamps
    end

    create_table :conversation_participations do |t|
      t.references :conversation, :null => false
      t.references :user,         :null => false
    end

    add_index :conversation_participations,
      [:user_id, :conversation_id],
      :unique => true,
      :name => 'conversation_participations_uniqueness'
    add_foreign_key :conversation_participations, :conversations
    add_foreign_key :conversation_participations, :users

    create_table :messages do |t|
      t.references :conversation_participation,  :null => false
      t.text       :content,                     :null => false
      t.timestamps
    end

    add_foreign_key :messages, :conversation_participations
  end
end
